#ifndef CONFIG_H
#define CONFIG_H

#include <QString>
#include "libs/version.h"

#include "apikey.h" // client_id and client_secret hidden here, ignored by git

//#define RELEASEBUILD

const Version AppVersion("0.4.1");
const QString OrganiztionName("BeTheRock");
const QString OrganiztionDomain("betherock.org");
const QString ApplicationName("com.ubuntu.developer.larochelle.brian.feed-the-aggregator");
const QString BinaryName("feed-the-aggregator");

const QString RedirectURL = "http://localhost";
//const QString RedirectURL = "urn:ietf:wg:oauth:2.0:oob";
const QString Scope = "https://cloud.feedly.com/subscriptions";
//const QString Scope = "";

#ifdef RELEASEBUILD
const QString ClientId = FeedlyClientId;
const QString ClientSecret = FeedlyClientSecret;
const QString FeedlyBase = "http://cloud.feedly.com/v3";
#else
const QString ClientId = FeedlyClientIdDev;
const QString ClientSecret = FeedlyClientSecretDev;
const QString FeedlyBase = "http://sandbox.feedly.com/v3";
#endif

// ***************  methods consts *************************
const QString FeedlyAuth          = QString("%1/auth/auth").arg(FeedlyBase);
const QString FeedlyAuthToken     = QString("%1/auth/token").arg(FeedlyBase);
const QString FeedlyPreferences   = QString("%1/preferences").arg(FeedlyBase);
const QString FeedlyProfile       = QString("%1/profile").arg(FeedlyBase);
const QString FeedlyCategories    = QString("%1/categories").arg(FeedlyBase);
const QString FeedlySubscriptions = QString("%1/subscriptions").arg(FeedlyBase);
const QString FeedlyMarkers       = QString("%1/markers/counts").arg(FeedlyBase);
const QString FeedlyFeeds		   = QString("%1/feeds/:feedId").arg(FeedlyBase);

//const QString FeedlyStream        = QString("%1/streams/").arg(FeedlyBase);
const QString FeedlyStream        = QString("%1/streams/ids?streamId=").arg(FeedlyBase);
const QString FeedlyStreamContent = QString("%1/streams/contents?streamId=").arg(FeedlyBase);
const QString FeedlyEntriesMget   = QString("%1/entries/.mget").arg(FeedlyBase);
const QString FeedlyMarkEntryRead = QString("%1/markers").arg(FeedlyBase);
const QString FeedlyMarkFeedRead = QString("%1/markers").arg(FeedlyBase);


#endif // CONFIG_H
