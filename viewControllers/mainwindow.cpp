// qt
#include <QDebug>
#include <QState>
#include <QSet>
#include <QQmlContext>
#include <QMultiMap>
#include <QProcessEnvironment>

// local
#include "mainwindow.h"
#include "libs/feedlyapi.h"
#include "config.h"

MainWindow::MainWindow()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(true);

    setNoFeedly(false);

    feedly = new FeedlyApi();

    setupUi();
    if(!mNoFeedly)
        initFeedly();
    else
        emit loadMainPage();
    setIsBusy(false);

}

// Q_INVOKABLE
bool MainWindow::isReleaseBuild() const
{
#ifdef RELEASEBUILD
    return true;
#else
    return false;
#endif
}

QString MainWindow::getAboutContents() const
{
    QFile f(QString(":/about.txt"));
    QString result = "";

    if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&f);
        result = in.readAll();
        f.close();
    }
    result.prepend(QString("Current Version %1\n\n").arg(AppVersion.toString()));
    return result;
}

QString MainWindow::getApplicationName() const
{
    return ApplicationName;
}

void MainWindow::refresh()
{
    setIsBusy(true);
    //feedly->getFeedsModel()->clear();
    //feedly->getEntriesModel()->clear();

    if(feedly->getCategories())
        if(feedly->getSubscriptions())
            feedly->getMarkers();
    setIsBusy(false);
}

// data model helpers
int MainWindow::getCategoryCurrentIndex() const
{
    return feedly->getFeedsModel()->currentIndex();
}

int MainWindow::getFeedCurrentIndex() const
{
    return feedly->getFeedsModel()->at(getCategoryCurrentIndex())->feedModel()->currentIndex();
}

int MainWindow::getEntriesCurrentIndex() const
{
    return feedly->getEntriesModel()->currentIndex();
}

void MainWindow::markEntryAsRead()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(true);
    int index = feedly->getEntriesModel()->currentIndex();

    if(isReleaseBuild()) {
        index = feedly->getEntriesModel()->currentIndex();
        QString entryId = feedly->getEntriesModel()->at(index)->entryId();
        feedly->markEntryAsRead(entryId);
    }

    feedly->getEntriesModel()->markEntryAsRead(index);
    index = feedly->getFeedsModel()->currentIndex();
    feedly->getFeedsModel()->markFeedAsRead(index);

    //NOTE:: no longer needed
    feedly->getFeedsModel()->subtractGlobalUnreadItems(1);
    setIsBusy(false);
}

void MainWindow::markFeedAsRead()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(true);
    int index = feedly->getFeedsModel()->currentIndex();

    if(isReleaseBuild()) {
        feedly->markFeedAsRead(feedly->getFeedsModel()->at(index)->id());
    }
    int count = feedly->getEntriesModel()->markAllEntriesRead();
    feedly->getFeedsModel()->markFeedsAsRead(index, count);
    //NOTE:: no longer needed
    feedly->getFeedsModel()->subtractGlobalUnreadItems(count);

    setIsBusy(false);
}

QString MainWindow::getEntryUrl() const
{
    QString url;
    int index = feedly->getEntriesModel()->currentIndex();
    url = feedly->getEntriesModel()->at(index)->alternativeLink();
    if(!url.isEmpty())
        return url;
    url = feedly->getEntriesModel()->at(index)->canonicalLink();
    return url;
}

// api methods
void MainWindow::getStream(const QString &id)
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(true);
    feedly->getStream(id);
    setIsBusy(false);
}

void MainWindow::getEntries(const QString &feedId)
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(true);
    if(feedly->getEntriesMgetFromFeedId(feedId)) {
        qDebug() << "get entries success";
        getView()->rootContext()->setContextProperty("entriesModel", feedly->getEntriesModel());
        emit loadEntriesList();
    }
    setIsBusy(false);
}


// setters
void MainWindow::setNoFeedly(const bool &trueFalse)
{
    mNoFeedly = trueFalse;
    emit noFeedlyChanged();
}

// getters
bool MainWindow::noFeedly() const
{
    return mNoFeedly;
}

// public slots
void MainWindow::gotAuthoriztion()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");

    feedly->setAccessToken(settings.value("access_token").toString());

    feedly->getProfile();
    feedly->getPreferences();
    if(feedly->getCategories())
        if(feedly->getSubscriptions())
            feedly->getMarkers();
                getView()->rootContext()->setContextProperty("categoryModel", feedly->getFeedsModel());

    setIsBusy(false);
    emit loadMainPage();
}

// private
void MainWindow::setupUi()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");

    //getView()->setResizeMode(QQuickView::SizeRootObjectToView);
    getView()->rootContext()->setContextProperty("mainwindow", this);
    getView()->rootContext()->setContextProperty("feedlyapi", feedly);
    getView()->rootContext()->setContextProperty("categoryModel", NULL);
    getView()->rootContext()->setContextProperty("entriesModel", NULL);


    getView()->setSource(QString("qrc:/qml/MainWindow.qml"));
    //getView()->resize(720, 1280);

    #if defined(Q_OS_MAC) || defined(Q_OS_LINUX) || defined(Q_OS_WIN32)
    getView()->show();
    #else
    getView()->showFullScreen();
    #endif
}

void MainWindow::initFeedly()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    connect(feedly, SIGNAL(authCompleted()), this, SLOT(gotAuthoriztion()));

    QString tempAccessToken = settings.value("access_token").toString();

    if(!tempAccessToken.isEmpty()) {
        //qDebug() << "token: " << tempAccessToken;
        feedly->setAccessToken(tempAccessToken);
        if(!feedly->getProfile()) {
            qDebug() << "error: " << feedly->getFeedlyError();
            tempAccessToken = settings.value("refresh_token").toString();
            feedly->setAccessToken(tempAccessToken);
            if(!feedly->getProfile()) {
                emit loadAuthPage();
                feedly->beginAuth();
                return;
            }
        }
    }
    else {
        qDebug() << "no token";
        emit loadAuthPage();
        feedly->beginAuth();
        return;
    }
    gotAuthoriztion();
}

void MainWindow::setupStateMachine()
{

    /*
    s1 = new QState(&stateMachine);
    s2 = new QState(&stateMachine);
    s3 = new QState(&stateMachine);

    s1->setObjectName("s1-state");
    s2->setObjectName("s2");
    s3->setObjectName("s3");

    //stateMachine.addState(s1);
    //stateMachine.addState(s2);
    //stateMachine.addState(s3);

    stateMachine.setInitialState(s1);
    stateMachine.start();
    */

}

void MainWindow::checkState()
{
    qDebug() << "checkState";
    qDebug() << "config: " << getStateMachine()->configuration();

    /*
    //jif(stateMachine.isRunning())
    //    qDebug() << "statemahicne running";
    //else
    //    qDebug() << "statemachine not running";


    //qDebug() << "error : " << stateMachine.errorString();
    //sleep(5);
    //qDebug() << "current state: " << stateMachine.configuration();


    if(stateMachine.configuration().contains(s1)) {
        qDebug() << "in s1";
    }
    else if(stateMachine.configuration().contains(s2)) {
        qDebug() << "in s2";
    }
    else if(stateMachine.configuration().contains(s3)) {
        qDebug() << "in s3";
    }
    else
        qDebug() << "farts";


   // if(stateMachine.configuration().contains(QString("s1")))
   //     qDebug() << "found name s1";

//    QSet(QState(0x9b5f158, name = "s1") )

    //QSet<QState*> set = stateMachine.configuration();

    qDebug() << "count: " << stateMachine.configuration().count();
    qDebug() << "toList: " << stateMachine.configuration().toList();
    qDebug() << "toList.at: " << stateMachine.configuration().toList().at(0);
    qDebug() << "toList.at objectname: " << stateMachine.configuration().toList().at(0)->objectName();
    qDebug() << "values: " << stateMachine.configuration().values();
*/
}










































