#ifndef VIEWCONTROLLER_H
#define VIEWCONTROLLER_H

#include <QObject>
#include <QQuickView>
#include <QStateMachine>

#include "libs/logger.h"

class ViewController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isBusy READ isBusy WRITE setIsBusy NOTIFY isBusyChanged)

public:
    explicit ViewController();
    //TODO:: kill copy and assigment ctors

    ~ViewController();
protected:

    void setIsBusy(const bool &isBusy);
    bool isBusy() const;

    Q_INVOKABLE void loggingFromQml(QString caller, QString message);
    virtual void setupUi();
    void addState(QState &state, QString &stateName);
    bool inline isInState(QState &state);
    bool inline isInState(QString &stateName);

    QQuickView* getView();
    QQmlContext* getRootcontext();
    QStateMachine* getStateMachine();
    Logger *logger;

signals:
    void isBusyChanged();

private:
    static QStateMachine *stateMachine;
    static QState *initialState;
    static QQuickView *view;
    static int instances;

    bool mIsBusy;
};

#endif // VIEWCONTROLLER_H
