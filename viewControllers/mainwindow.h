#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QSettings>

#include "viewcontroller.h"


class CategoryModel;
class FeedlyApi;
//class KQOAuthManager;
//class KQOAuthRequest;

class MainWindow : public ViewController
{
    Q_OBJECT
    Q_PROPERTY(bool noFeedly READ noFeedly WRITE setNoFeedly NOTIFY noFeedlyChanged)
public:
    explicit MainWindow();
    
    Q_INVOKABLE bool isReleaseBuild() const;
    Q_INVOKABLE QString getAboutContents() const;
    Q_INVOKABLE QString getApplicationName() const;
    Q_INVOKABLE void refresh();
    // data model helpers
    Q_INVOKABLE int getFeedCurrentIndex() const;
    Q_INVOKABLE int getCategoryCurrentIndex() const;
    Q_INVOKABLE int getEntriesCurrentIndex() const;
    Q_INVOKABLE void markEntryAsRead();
    Q_INVOKABLE void markFeedAsRead();
    Q_INVOKABLE QString getEntryUrl() const;

    // api methods
    Q_INVOKABLE void getStream(const QString &id);
    Q_INVOKABLE void getEntries(const QString &feedId);

    void setNoFeedly(const bool &trueFalse);
    bool noFeedly() const;
signals:
    void loadAuthPage();
    void loadMainPage();
    void loadEntriesList();
    void noFeedlyChanged();
public slots:
    void gotAuthoriztion();

private:
    void setupUi();
    void initFeedly();
    void setupStateMachine();
    void checkState();

    FeedlyApi *feedly;
    QSettings settings;
    int mCategoryIndex;
    int mEntryIndex;

    bool mNoFeedly;

    //QState *s1;
    //QState *s2;
    //QState *s3;
};

#endif // MAINWINDOW_H
