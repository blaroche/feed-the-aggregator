#include <QDebug>
#include <QtQml/QQmlEngine>
#include <QQmlContext>

#include "viewcontroller.h"

QStateMachine *ViewController::stateMachine = new QStateMachine();
QState *ViewController::initialState = NULL;
QQuickView *ViewController::view = NULL;
int ViewController::instances = 0;

ViewController::ViewController()
{
    logger = Logger::instance();
    logger->enableConsoleDebug(true);
    logger->enableLogFileDebug(false);
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    setIsBusy(false);
    instances++;

    if(view == NULL)
        view = new QQuickView();;

    if(initialState == NULL) {
        initialState = new QState(stateMachine);
        initialState->setObjectName("initialState");
        stateMachine->setInitialState(initialState);
        stateMachine->start();
    }
}

ViewController::~ViewController()
{
    instances--;
    if(instances == 0) {
        if(stateMachine != NULL) {
            delete stateMachine;
            stateMachine = NULL;
        }
        if(view != NULL) {
            delete view;
            view = NULL;
        }
    }
}

void ViewController::setIsBusy(const bool &isBusy)
{
    mIsBusy = isBusy;
    emit isBusyChanged();
}

bool ViewController::isBusy() const
{
    return mIsBusy;
}

void ViewController::loggingFromQml(QString caller, QString message)
{
    logger->debug(caller, message);
}

void ViewController::addState(QState &state, QString &stateName)
{
    if(stateMachine->configuration().contains(&state) ) {
        qDebug() << "state already added";
        return;
    }
    state.setObjectName(stateName);
    stateMachine->addState(&state);
}

bool ViewController::isInState(QState &state)
{
    if(stateMachine->configuration().contains(&state))
        return true;
    return false;
}

bool ViewController::isInState(QString &stateName)
{
    if(stateMachine->configuration().toList().at(0)->objectName() == stateName)
        return true;
    return false;
}

QQuickView* ViewController::getView()
{
    return view;
}

QQmlContext* ViewController::getRootcontext()
{
    return view->rootContext();
}

QStateMachine* ViewController::getStateMachine()
{
    return stateMachine;
}

void ViewController::setupUi()
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    //view->rootContext()->setContextProperty("mainwindow", this);
    view->setSource(QString("qrc:/qml/Feedly.qml"));

    #if defined(Q_WS_SIMULATOR)
    view->showFullScreen();
    #else
    view->show();
    #endif
}















































