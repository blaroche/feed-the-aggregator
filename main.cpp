#include <QtGui/QGuiApplication>

#include "viewControllers/mainwindow.h"
#include "config.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName(ApplicationName);
    MainWindow main;
    Q_UNUSED(main);

    return app.exec();
}
