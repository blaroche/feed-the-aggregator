
#include "models/profilemodel.h"

#include "parseprofiletest.h"


void ParseProfileTest::initTestCase()
{
    openJsonFile("profileResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Profile);
}

void ParseProfileTest::cleanupTestCase()
{
}

void ParseProfileTest::init()
{
}

void ParseProfileTest::cleanup()
{
}



// unit tests
void ParseProfileTest::parseClient()
{
    QCOMPARE(parser.getProfileModel()->client(), QString("sandbox216"));
}

void ParseProfileTest::parseEmail()
{
    QCOMPARE(parser.getProfileModel()->email(), QString("larochelle.brian@gmail.com"));
}

void ParseProfileTest::parseFullName()
{
    QCOMPARE(parser.getProfileModel()->fullName(), QString("brian larochelle"));
}

void ParseProfileTest::parseGender()
{
    QCOMPARE(parser.getProfileModel()->gender(), QString("male"));
}

void ParseProfileTest::parseWordPressConnected()
{
    QCOMPARE(parser.getProfileModel()->wordPressConnected(), false);
}

void ParseProfileTest::parseEvernoteConnected()
{
    QCOMPARE(parser.getProfileModel()->evernoteConnected(), false);
}

void ParseProfileTest::parseFamilyName()
{
    QCOMPARE(parser.getProfileModel()->familyName(), QString("larochelle"));
}

void ParseProfileTest::parseGivenName()
{
    QCOMPARE(parser.getProfileModel()->givenName(), QString("brian"));
}

void ParseProfileTest::parseGoogle()
{
    QCOMPARE(parser.getProfileModel()->google(), QString("105702319917846808322"));
}

void ParseProfileTest::parseId()
{
    QCOMPARE(parser.getProfileModel()->google(), QString("105702319917846808322"));
}

void ParseProfileTest::parseLocale()
{

    QCOMPARE(parser.getProfileModel()->locale(), QString("en"));
}

void ParseProfileTest::parsePicture()
{
    QCOMPARE(parser.getProfileModel()->picture(), QString("https://lh3.googleusercontent.com/-HNwTmm48igQ/AAAAAAAAAAI/AAAAAAAAAJA/0dFQai-1NL0/photo.jpg?sz=50"));
}

void ParseProfileTest::parsePocketConnected()
{
    QCOMPARE(parser.getProfileModel()->pocketConnected(), false);
}

void ParseProfileTest::parseWave()
{
    QCOMPARE(parser.getProfileModel()->wave(), QString("2013.38"));
}



// private
void ParseProfileTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}


