#ifndef FEEDANDENTRYMODELTEST_H
#define FEEDANDENTRYMODELTEST_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"
#include "models/feedsmodel.h"
#include "models/entriesmodel.h"

class FeedAndEntryModelTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit tests
    void firstTest();

private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;

};

#endif
