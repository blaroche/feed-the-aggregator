

#include "models/feedsmodel.h"

#include "parsemarkerstest.h"


void ParseMarkersTest::initTestCase()
{
    // need to parse and create category model before
    // we can add the subscriptions sub model
    openJsonFile("categoriesResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Categories);
    // need to parse subscriptions before adding unread markers
    openJsonFile("subscriptionsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Subscriptions);


    openJsonFile("markersResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Markers);
}

void ParseMarkersTest::cleanupTestCase()
{

}

void ParseMarkersTest::init()
{
}

void ParseMarkersTest::cleanup()
{
}

// unit tests
void ParseMarkersTest::globalAllUnreadCountIsCorrect()
{
    //QCOMPARE(parser.getC
    QCOMPARE(parser.getCategoryModel()->globalUnreadItems(), 1362.0);
}

void ParseMarkersTest::globalUncategorizedUnreadCountIsCorrect()
{
    QString id = "global.uncategorized";
    QCOMPARE(parser.getCategoryModel()->findCategoryById(id)->unreadCount(), 127.0);
}

// verify feed unread counts
void ParseMarkersTest::unreadCountOnAseigoFeedIdIsCorrect()
{
    QString id = "feed/http://aseigo.blogspot.com/feeds/posts/default";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 6.0);
    }
}

void ParseMarkersTest::unreadCountOnAppleInsiderIsCorrect()
{
    QString id = "feed/http://appleinsider.com.feedsportal.com/c/33975/f/616168/index.rss";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 268.0);
    }
}

void ParseMarkersTest::unreadCountOnArsTechnicaIsCorrect()
{
    QString id = "feed/http://feeds.arstechnica.com/arstechnica/everything";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 402.0);
    }
}

void ParseMarkersTest::unreadCountOnPhoronixIsCorrect()
{
    QString id = "feed/http://www.phoronix.com/rss.php";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 324.0);
    }
}

void ParseMarkersTest::unreadCountOnTiGraphicsIsCorrect()
{
    QString id = "feed/http://tigraphics.blogspot.com/feeds/posts/default";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 1.0);
    }
}

void ParseMarkersTest::unreadCountOnZchydemIsCorrect()
{
    QString id = "feed/http://zchydem.enume.net/feed/";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 0.0);
    }
}

void ParseMarkersTest::unreadCountOnCanonicalJobsIsCorrect()
{
    QString id = "feed/https://tbe.taleo.net/NA5/ats/servlet/Rss?org=MTVNETWORKS&cws=1&WebPage=SRCHR&WebVersion=0&_rss_version=2";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 72.0);
    }
}

void ParseMarkersTest::unreadCountOnOatmeailIsCorrect()
{
    QString id = "feed/http://theoatmeal.com/feed/rss";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 3.0);
    }
}

void ParseMarkersTest::unreadCountOnCrackedIsCorrect()
{
    QString id = "feed/http://feeds.feedburner.com/CrackedRSS";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 159.0);
    }
}

void ParseMarkersTest::unreadCountOnPlanetKdeIsCorrect()
{
    QString id = "feed/http://planetkde.org/rss20.xml";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 121.0);
    }
}

void ParseMarkersTest::unreadCountOnJonoBaconIsCorrect()
{
    QString id = "feed/http://www.jonobacon.org/?feed=rss2";
    QList<Feed* > feeds = parser.getCategoryModel()->findFeedsById(id);

    foreach(Feed *feed, feeds) {
        QCOMPARE(feed->unreadCount(), 6.0);
    }
}


// verify category unread counts
void ParseMarkersTest::unreadCountOnNewsCategoryIsCorrect()
{
    QString id = "user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/News";
    QCOMPARE(parser.getCategoryModel()->findCategoryById(id)->unreadCount(), 1000.0);
}

void ParseMarkersTest::unreadCountOnTestingCategoryIsCorrect()
{
    QString id = "user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/testing";
    QCOMPARE(parser.getCategoryModel()->findCategoryById(id)->unreadCount(), 6.0);
}

void ParseMarkersTest::unreadCountOnQtCategoryIsCorrect()
{
    QString id = "user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/Qt";
    QCOMPARE(parser.getCategoryModel()->findCategoryById(id)->unreadCount(), 1.0);
}

void ParseMarkersTest::unreadCountOnXmiscCategoryIsCorrect()
{
    QString id = "user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/X-misc";
    QCOMPARE(parser.getCategoryModel()->findCategoryById(id)->unreadCount(), 234.0);
}


// private

void ParseMarkersTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}










