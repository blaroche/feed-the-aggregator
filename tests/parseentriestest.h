#ifndef PARSEENTRIES_H
#define PARSEENTRIES_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"
#include "models/entriesmodel.h"

class ParseEntriesTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit tests
    void verifyFirstTiGraphicsEntry();
    void veriryLastTiGraphicsEntry();

    void verifyFirstAseigoEntry();
    void veriryLastAseigoEntry();

private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;
};



#endif



