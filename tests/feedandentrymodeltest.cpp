
#include "feedandentrymodeltest.h"

void FeedAndEntryModelTest::initTestCase()
{
}

void FeedAndEntryModelTest::cleanupTestCase()
{
}

void FeedAndEntryModelTest::init()
{
    // load the models with data
    openJsonFile("categoriesResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Categories);
    openJsonFile("subscriptionsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Subscriptions);
    openJsonFile("markersResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Markers);
    openJsonFile("entriesMGetAseigoResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::EntriesMget);
}

void FeedAndEntryModelTest::cleanup()
{
}


// unit tests
void FeedAndEntryModelTest::firstTest()
{

}



//private
void FeedAndEntryModelTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}
