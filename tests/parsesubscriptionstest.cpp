
#include "parsesubscriptionstest.h"
#include "models/feedsmodel.h"

void ParseSubscriptionsTest::initTestCase()
{
    // need to parse and create category model before
    // we can add the subscriptions sub model
    openJsonFile("categoriesResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Categories);

    openJsonFile("subscriptionsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Subscriptions);
}

void ParseSubscriptionsTest::cleanupTestCase()
{

}

void ParseSubscriptionsTest::init()
{
}

void ParseSubscriptionsTest::cleanup()
{
}

// unit tests
void ParseSubscriptionsTest::categoriesStillFlaggedAsCategories()
{
    for(int i = 0; i < parser.getFeedsModel()->rowCount(); i++) {
        Feed *category = NULL;
        category = parser.getFeedsModel()->at(i);
        QCOMPARE(category->isCategory(), true);
    }
}

void ParseSubscriptionsTest::feedsAreFlaggedAsSubscriptions()
{
    for(int i = 0; i < parser.getFeedsModel()->rowCount(); i++) {
        Feed *category = NULL;
        category = parser.getFeedsModel()->at(i);
        //QCOMPARE(category->isCategory(), true);

        for(int j = 0; j < category->feedModel()->rowCount(); j++) {
            Feed *feed = NULL;
            feed = category->feedModel()->at(j);
            //qDebug() << "label: " << feed->label();
            QCOMPARE(feed->isSubscription(), true);
        }
    }
}

void ParseSubscriptionsTest::verifyFirstCategoryFirstItem()
{
    Feed *feed = parser.getFeedsModel()->at(0)->feedModel()->at(0);

    QCOMPARE(feed->label(), QString("aseigo"));
    QCOMPARE(feed->id(), QString("feed/http://aseigo.blogspot.com/feeds/posts/default"));
    QCOMPARE(feed->webSite(), QString("http://aseigo.blogspot.com/"));
    //QCOMPARE(feed->updated(), 1386067180871.0);
    //QCOMPARE(feed->velocity(), 1.3999999999999999);
}

void ParseSubscriptionsTest::verifySecondCategoryFirstItem()
{
    Feed *feed = parser.getFeedsModel()->at(1)->feedModel()->at(0);

    QCOMPARE(feed->label(), QString("aseigo"));
    QCOMPARE(feed->id(), QString("feed/http://aseigo.blogspot.com/feeds/posts/default"));
    QCOMPARE(feed->webSite(), QString("http://aseigo.blogspot.com/"));
    //QCOMPARE(feed->updated(), 1386067180871.0);
    //QCOMPARE(feed->velocity(), 1.3999999999999999);
}

void ParseSubscriptionsTest::verifySecondCategorySecondItem()
{
    Feed *feed = parser.getFeedsModel()->at(1)->feedModel()->at(1);

    QCOMPARE(feed->label(), QString("AppleInsider - Frontpage News"));
    QCOMPARE(feed->id(), QString("feed/http://appleinsider.com.feedsportal.com/c/33975/f/616168/index.rss"));
    QCOMPARE(feed->webSite(), QString("http://appleinsider.com"));
    //QCOMPARE(feed->updated(), 1387473589718.0);
    //QCOMPARE(feed->velocity(), 66.5);
}

void ParseSubscriptionsTest::verifySecondCategoryThirdItem()
{
    Feed *feed = parser.getFeedsModel()->at(1)->feedModel()->at(2);

    QCOMPARE(feed->label(), QString("Ars Technica"));
    QCOMPARE(feed->id(), QString("feed/http://feeds.arstechnica.com/arstechnica/everything"));
    QCOMPARE(feed->webSite(), QString("http://arstechnica.com"));
    //QCOMPARE(feed->updated(), 1387472921041.0);
    //QCOMPARE(feed->velocity(), 114.3);
}

void ParseSubscriptionsTest::verifySecondCategoryFourthItem()
{
    Feed *feed = parser.getFeedsModel()->at(1)->feedModel()->at(3);

    QCOMPARE(feed->label(), QString("Phoronix"));
    QCOMPARE(feed->id(), QString("feed/http://www.phoronix.com/rss.php"));
    QCOMPARE(feed->webSite(), QString("http://www.phoronix.com/"));
    //QCOMPARE(feed->updated(), 1387226458785.0);
    //QCOMPARE(feed->velocity(), 73.5);
}

void ParseSubscriptionsTest::verifyThirdCategoryFirstItem()
{
    Feed *feed = parser.getFeedsModel()->at(2)->feedModel()->at(0);

    QCOMPARE(feed->label(), QString("TIGRAPHICS"));
    QCOMPARE(feed->id(), QString("feed/http://tigraphics.blogspot.com/feeds/posts/default"));
    QCOMPARE(feed->webSite(), QString("http://tigraphics.blogspot.com/"));
    //QCOMPARE(feed->updated(), 1384928928609.0);
    //QCOMPARE(feed->velocity(), 0.2);
}

void ParseSubscriptionsTest::verifyThirdCategorySecondItem()
{
    Feed *feed = parser.getFeedsModel()->at(2)->feedModel()->at(1);

    QCOMPARE(feed->label(), QString("Zchydem's Blog » Feed"));
    QCOMPARE(feed->id(), QString("feed/http://zchydem.enume.net/feed/"));
    QCOMPARE(feed->webSite(), QString("http://zchydem.enume.net"));
    //QCOMPARE(feed->updated(), 1359971465000.0);
    //QCOMPARE(feed->velocity(), 0.0);
}

void ParseSubscriptionsTest::verifyFourthCategoryFirstItem()
{
    Feed *feed = parser.getFeedsModel()->at(3)->feedModel()->at(0);

    QCOMPARE(feed->label(), QString("Viacom Job Feed"));
    QCOMPARE(feed->id(), QString("feed/https://tbe.taleo.net/NA5/ats/servlet/Rss?org=MTVNETWORKS&cws=1&WebPage=SRCHR&WebVersion=0&_rss_version=2"));
    QCOMPARE(feed->webSite(), QString("http://ch.tbe.taleo.net/CH05/ats/careers/jobSearch.jsp?org=MTVNETWORKS&cws=1"));
    //QCOMPARE(feed->updated(), 1387229576973.0);
    //QCOMPARE(feed->velocity(), 12.8);
}

void ParseSubscriptionsTest::verifyFourthCategorySecondItem()
{
    Feed *feed = parser.getFeedsModel()->at(3)->feedModel()->at(1);

    QCOMPARE(feed->label(), QString("The Oatmeal - Comics, Quizzes, & Stories"));
    QCOMPARE(feed->id(), QString("feed/http://theoatmeal.com/feed/rss"));
    QCOMPARE(feed->webSite(), QString("http://theoatmeal.com/"));
    //QCOMPARE(feed->updated(), 1385707384978.0);
    //QCOMPARE(feed->velocity(), 0.9);
}

void ParseSubscriptionsTest::verifyFourthCategoryThirdItem()
{
    Feed *feed = parser.getFeedsModel()->at(3)->feedModel()->at(2);

    QCOMPARE(feed->label(), QString("Cracked: All Posts"));
    QCOMPARE(feed->id(), QString("feed/http://feeds.feedburner.com/CrackedRSS"));
    QCOMPARE(feed->webSite(), QString("http://www.cracked.com"));
    //QCOMPARE(feed->updated(), 1387200806476.0);
    //QCOMPARE(feed->velocity(), 42.0);
}

void ParseSubscriptionsTest::verifyFirstFeedWithNoCategory()
{
    Feed *feed = parser.getFeedsModel()->at(4)->feedModel()->at(0);

    QCOMPARE(feed->label(), QString("Planet KDE"));
    QCOMPARE(feed->id(), QString("feed/http://planetkde.org/rss20.xml"));
    QCOMPARE(feed->webSite(), QString("http://planetKDE.org/"));
    //QCOMPARE(feed->updated(), 1387224004613.0);
    //QCOMPARE(feed->velocity(), 33.8);
}

void ParseSubscriptionsTest::verifySecondFeedWithNoCategory()
{
    Feed *feed = parser.getFeedsModel()->at(4)->feedModel()->at(1);

    QCOMPARE(feed->label(), QString("jonobacon@home"));
    QCOMPARE(feed->id(), QString("feed/http://www.jonobacon.org/?feed=rss2"));
    QCOMPARE(feed->webSite(), QString("http://www.jonobacon.org"));
    //QCOMPARE(feed->updated(), 1386067180871.0);
    //QCOMPARE(feed->velocity(), 1.2);
}


//private
void ParseSubscriptionsTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}



























