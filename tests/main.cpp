#include <QtGui/QGuiApplication>
//#include <QCoreApplication>
#include <QtTest>

#include "config.h"
#include "libs/logger.h"


#include "feedlyapitest.h"
#include "parseprofiletest.h"
#include "parsecategoriestest.h"
#include "parsesubscriptionstest.h"
#include "parsemarkerstest.h"
#include "parseentriestest.h"
#include "feedmodeltest.h"
#include "feedandentrymodeltest.h"

int main(int argc, char** argv) {
    //QCoreApplication app(argc, argv);
    QGuiApplication app(argc, argv);
    app.setOrganizationName(ApplicationName);
    app.setApplicationName(BinaryName);

    Logger *logger = Logger::instance();
    logger->enableConsoleDebug(true);


    //FeedlyApiTest feedlyApiTest;
    ParseProfileTest parseProfileTest;
    ParseCategoriesTest parseCategoriesTest;
    ParseSubscriptionsTest parseSubscriptionsTest;
    ParseMarkersTest parseMarkersTest;
    ParseEntriesTest parseEntriesTest;
    //FeedModelTest feedModelTest;
    FeedAndEntryModelTest feedAndEntryModelTest;

    //QTest::qExec(&feedlyApiTest, argc, argv);
    QTest::qExec(&parseProfileTest,  argc, argv);
    QTest::qExec(&parseCategoriesTest, argc, argv);
    QTest::qExec(&parseSubscriptionsTest, argc, argv);
    QTest::qExec(&parseMarkersTest, argc, argv);
    QTest::qExec(&parseEntriesTest, argc, argv);
    //QTest::qExec(&feedModelTest, argc, argv);
    QTest::qExec(&feedAndEntryModelTest, argc, argv);

    return 0;
}
