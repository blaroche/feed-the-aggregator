
#include "models/feedsmodel.h"

#include "parsecategoriestest.h"


void ParseCategoriesTest::initTestCase()
{
    openJsonFile("categoriesResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Categories);
}

void ParseCategoriesTest::cleanupTestCase()
{

}

void ParseCategoriesTest::init()
{
}

void ParseCategoriesTest::cleanup()
{
}

// unit tests
void ParseCategoriesTest::testDataShouldHaveFourCategories()
{
   QCOMPARE(parser.getCategoryModel()->rowCount(), 4);
}

void ParseCategoriesTest::globalUnreadCountShouldBeZero()
{
   QCOMPARE(parser.getCategoryModel()->globalUnreadItems(), 0.0);
}

void ParseCategoriesTest::itemsAreFlaggedAsCategories()
{
   QCOMPARE(parser.getCategoryModel()->at(0)->isCategory(), true);
   QCOMPARE(parser.getCategoryModel()->at(1)->isCategory(), true);
   QCOMPARE(parser.getCategoryModel()->at(2)->isCategory(), true);
   QCOMPARE(parser.getCategoryModel()->at(3)->isCategory(), true);
}

void ParseCategoriesTest::firstItemHasCorrectIdAndLabel()
{
   QCOMPARE(parser.getCategoryModel()->at(0)->id(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/testing"));
   QCOMPARE(parser.getCategoryModel()->at(0)->label(), QString("testing"));
}

void ParseCategoriesTest::secondItemHasCorrectIdAndLabel()
{
   QCOMPARE(parser.getCategoryModel()->at(1)->id(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/News"));
   QCOMPARE(parser.getCategoryModel()->at(1)->label(), QString("News"));
}

void ParseCategoriesTest::thirdItemHasCorrectIdAndLabel()
{
   QCOMPARE(parser.getCategoryModel()->at(2)->id(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/Qt"));
   QCOMPARE(parser.getCategoryModel()->at(2)->label(), QString("Qt"));
}

void ParseCategoriesTest::fourthItemHasCorrectIdAndLabel()
{
   QCOMPARE(parser.getCategoryModel()->at(3)->id(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/X-misc"));
   QCOMPARE(parser.getCategoryModel()->at(3)->label(), QString("X-misc"));
}





// private
void ParseCategoriesTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}

