#include <QDebug>
#include "parseentriestest.h"



void ParseEntriesTest::initTestCase()
{
    //openJsonFile("entriesMGetTiGraphicsResponse.json");
    //parser.parseResponse(jsonString, FeedlyParser::EntriesMget);
}

void ParseEntriesTest::cleanupTestCase()
{

}

void ParseEntriesTest::init()
{
}

void ParseEntriesTest::cleanup()
{
}



// unit tests
void ParseEntriesTest::verifyFirstTiGraphicsEntry()
{
    openJsonFile("entriesMGetTiGraphicsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::EntriesMget);

    Entry *entry = parser.getEntriesModel()->at(0);
    QCOMPARE(entry->alternativeLink(), QString("http://tigraphics.blogspot.com/2013/11/4q-2013-linux-graphics-sdk-release.html"));
    QCOMPARE(entry->alternativeType(), QString("text/html"));
    QCOMPARE(entry->author(), QString("Prathap Srinivas"));
    QCOMPARE(entry->categoryLabel(), QString("Qt"));
    QCOMPARE(entry->categoryId(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/Qt"));
    // skip content QCOMPARE(entry->(), QString(""));
    QCOMPARE(entry->crawled(), 1384928928609.0);
    QCOMPARE(entry->fingerprint(), QString("e863672b"));
    QCOMPARE(entry->entryId(), QString("spo7r7bbAmM/9CKZdOjC8sQcGfTmB8f//47bih3MAP4=_14274326361:78a:1196d0bf"));
    QCOMPARE(entry->originHtmlUrl(), QString("http://tigraphics.blogspot.com/"));
    QCOMPARE(entry->originTitle(), QString("TIGRAPHICS"));
    QCOMPARE(entry->streamId(), QString("feed/http://tigraphics.blogspot.com/feeds/posts/default"));
    QCOMPARE(entry->originId(), QString("tag:blogger.com,1999:blog-6511639843549834846.post-5318075079042026369"));
    QCOMPARE(entry->published(), 1384928880000.0);
    QCOMPARE(entry->recrawled(), 1384929693876.0);
    QCOMPARE(entry->sid(), QString("0:niIG7BE1XcOuRRzBGY/1T03ocJIjZdONNk51Te575tc="));
    QCOMPARE(entry->title(), QString("4Q 2013 Linux Graphics SDK release update - Linux Graphics SDK alpha release 05.00.00.01"));
    QCOMPARE(entry->unread(), true);
    QCOMPARE(entry->updated(), 1384929692450.0);
    //QCOMPARE(entry->(), QString(""));
}

void ParseEntriesTest::veriryLastTiGraphicsEntry()
{
    openJsonFile("entriesMGetTiGraphicsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::EntriesMget);

    Entry *entry = parser.getEntriesModel()->at(19);
    QCOMPARE(entry->alternativeLink(), QString("http://tigraphics.blogspot.com/2012/12/4q-2012-update-of-linux-graphics-sdk.html"));
    QCOMPARE(entry->alternativeType(), QString("text/html"));
    QCOMPARE(entry->author(), QString("Prathap Srinivas"));
    QCOMPARE(entry->categoryLabel(), QString("Qt"));
    QCOMPARE(entry->categoryId(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/Qt"));
    QCOMPARE(entry->crawled(), 1355396640000.0);
    QCOMPARE(entry->fingerprint(), QString("d9760078"));
    QCOMPARE(entry->entryId(), QString("spo7r7bbAmM/9CKZdOjC8sQcGfTmB8f//47bih3MAP4=_13b93ef6d00:191:14115a85"));
    QCOMPARE(entry->originHtmlUrl(), QString("http://tigraphics.blogspot.com/"));
    QCOMPARE(entry->streamId(), QString("feed/http://tigraphics.blogspot.com/feeds/posts/default"));
    QCOMPARE(entry->originTitle(), QString("TIGRAPHICS"));
    QCOMPARE(entry->originId(), QString("tag:blogger.com,1999:blog-6511639843549834846.post-4034624789668834729"));
    QCOMPARE(entry->published(), 1355396640000.0);
    QCOMPARE(entry->recrawled(), 0.0);
    QCOMPARE(entry->sid(), QString(""));
    QCOMPARE(entry->title(), QString("4Q 2012 update of Linux Graphics SDK Release"));
    QCOMPARE(entry->unread(), false);
    QCOMPARE(entry->updated(), 1361248182387.0);
    //QCOMPARE(entry->(), QString(""));
}

void ParseEntriesTest::verifyFirstAseigoEntry()
{
    openJsonFile("entriesMGetAseigoResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::EntriesMget);

    Entry *entry = parser.getEntriesModel()->at(0);

    QCOMPARE(entry->alternativeLink(), QString("http://aseigo.blogspot.com/2013/12/306.html"));
    QCOMPARE(entry->alternativeType(), QString("text/html"));
    QCOMPARE(entry->author(), QString("Aaron Seigo"));
    QCOMPARE(entry->categoryLabel(), QString("News"));
    QCOMPARE(entry->categoryId(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/News"));
    QCOMPARE(entry->crawled(), 1386067180871.0);
    QCOMPARE(entry->fingerprint(), QString("a30e6a2a"));
    QCOMPARE(entry->entryId(), QString("EqMjL2RC5MaSEwmWxAl9yTlxUoADHZ5rKP76WkaKic0=_142b80abd47:3036:ebf349f8"));
    QCOMPARE(entry->originHtmlUrl(), QString("http://aseigo.blogspot.com/"));
    QCOMPARE(entry->streamId(), QString("feed/http://aseigo.blogspot.com/feeds/posts/default"));
    QCOMPARE(entry->originTitle(), QString("aseigo"));
    QCOMPARE(entry->originId(), QString("tag:blogger.com,1999:blog-7615673.post-8379825862322152040"));
    QCOMPARE(entry->published(), 1386067140003.0);
    QCOMPARE(entry->recrawled(), 0.0);
    QCOMPARE(entry->sid(), QString("0:75tItrmdpRpBMKP4gUWgsgqx9c1j+/HY7Ke2/gwbCC4="));
    QCOMPARE(entry->title(), QString("30.6%"));
    QCOMPARE(entry->unread(), true);
    QCOMPARE(entry->updated(), 1386067178719.0);
}

void ParseEntriesTest::veriryLastAseigoEntry()
{
    openJsonFile("entriesMGetAseigoResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::EntriesMget);

    Entry *entry = parser.getEntriesModel()->at(19);

    QCOMPARE(entry->alternativeLink(), QString("http://aseigo.blogspot.com/2013/05/theming-plasma.html"));
    QCOMPARE(entry->alternativeType(), QString("text/html"));
    QCOMPARE(entry->author(), QString("Aaron Seigo"));
    QCOMPARE(entry->categoryLabel(), QString("News"));
    QCOMPARE(entry->categoryId(), QString("user/d5f1c6f6-a7fe-4e81-9333-3aa95f0ffb18/category/News"));
    QCOMPARE(entry->crawled(), 1369913760004.0);
    QCOMPARE(entry->fingerprint(), QString("a8a53bf8"));
    QCOMPARE(entry->entryId(), QString("EqMjL2RC5MaSEwmWxAl9yTlxUoADHZ5rKP76WkaKic0=_13ef5391904:40c:1219da3c"));
    QCOMPARE(entry->originHtmlUrl(), QString("http://aseigo.blogspot.com/"));
    QCOMPARE(entry->streamId(), QString("feed/http://aseigo.blogspot.com/feeds/posts/default"));
    QCOMPARE(entry->originTitle(), QString("aseigo"));
    QCOMPARE(entry->originId(), QString("tag:blogger.com,1999:blog-7615673.post-3532615378671659469"));
    QCOMPARE(entry->published(), 1369913760004.0);
    QCOMPARE(entry->recrawled(), 0.0);
    QCOMPARE(entry->sid(), QString(""));
    QCOMPARE(entry->title(), QString("theming plasma"));
    QCOMPARE(entry->unread(), false);
    QCOMPARE(entry->updated(), 1369920127757.0);
}


// private
void ParseEntriesTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}
