#ifndef PARSEMARKERS_H
#define PARSEMARKERS_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"

class ParseMarkersTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit tests
    void globalAllUnreadCountIsCorrect();
    void globalUncategorizedUnreadCountIsCorrect();

    // verify feed unread counts
    void unreadCountOnAseigoFeedIdIsCorrect();
    void unreadCountOnAppleInsiderIsCorrect();
    void unreadCountOnArsTechnicaIsCorrect();
    void unreadCountOnPhoronixIsCorrect();
    void unreadCountOnTiGraphicsIsCorrect();
    void unreadCountOnZchydemIsCorrect();
    void unreadCountOnCanonicalJobsIsCorrect();
    void unreadCountOnOatmeailIsCorrect();
    void unreadCountOnCrackedIsCorrect();
    void unreadCountOnPlanetKdeIsCorrect();
    void unreadCountOnJonoBaconIsCorrect();

    // verify category unread counts
    void unreadCountOnNewsCategoryIsCorrect();
    void unreadCountOnTestingCategoryIsCorrect();
    void unreadCountOnQtCategoryIsCorrect();
    void unreadCountOnXmiscCategoryIsCorrect();


private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;
};


#endif



