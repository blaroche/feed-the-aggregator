#ifndef FEEDMODELTEST_H
#define FEEDMODELTEST_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"
#include "models/feedsmodel.h"

class FeedModelTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit tests
    void findTheFirstFeedByFeedId();
    void findAllFeedsByFeedId();

    void decreaseUnreadByFeedIndexByOne();
    void decreaseUnreadByFeedIndexByEight();


private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;
};



#endif
