#ifndef FEEDLYAPITEST_H
#define FEEDLYAPITEST_H

#include <QDebug>
#include <QtTest/QtTest>

#include "libs/feedlyapi.h"


/*
This is a bad test suit.  The test should not depend on outside resources
and should make use of mocks, which this does not.  These tests are used
only for driving development, obtaining test data, and such.
This suit would normally be disabled.

since this is a bad test suit, initTestCase is a bad test method
is assumes a valid token is available via QSettings.
if fails, generate new token by running regualar application.
filthy hobbitses
*/


class FeedlyApiTest: public QObject
{
    Q_OBJECT
private slots:
    // functions executed by QtTest before and after test suite
    void initTestCase();
    void cleanupTestCase();
    // functions executed by QtTest before and after each test
    void init();
    void cleanup();

    // unit tests
    void beginAuthShouldReturnTrue();
    void accessTokenIsNotEmpty();
    void profileRequestShouldReturnTrue();
    void preferencesRequestShouldReturnTrue();
    void categoriesRequestShouldReturnTrue();
    void subscriptionsRequestShouldReturnTrue();
    void markersRequestShouldReturnTrue();
    void entriesMgetByFromFeedIdShouldReturnTrue();

private:
    FeedlyApi *feedly;
    QString accessToken;
};

#endif













