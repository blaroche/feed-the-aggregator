
#include "feedmodeltest.h"



void FeedModelTest::initTestCase()
{
}

void FeedModelTest::cleanupTestCase()
{
}

void FeedModelTest::init()
{
    // need to populate the FeedsModel with categories, subscriptsions, and markers
    openJsonFile("categoriesResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Categories);
    openJsonFile("subscriptionsResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Subscriptions);
    openJsonFile("markersResponse.json");
    parser.parseResponse(jsonString, FeedlyParser::Markers);
}

void FeedModelTest::cleanup()
{
}



// unit test
void FeedModelTest::findTheFirstFeedByFeedId()
{
    Feed *feed = parser.getCategoryModel()->findFirstFeedById("feed/http://feeds.arstechnica.com/arstechnica/everything");
    QVERIFY(feed != NULL);
    QCOMPARE(feed->label(), QString("Ars Technica"));
}

void FeedModelTest::findAllFeedsByFeedId()
{
    QList<Feed *> feeds = parser.getCategoryModel()->findFeedsById("feed/http://aseigo.blogspot.com/feeds/posts/default");
    QCOMPARE(feeds.count(), 2);
    QCOMPARE(feeds.at(0)->label(), QString("aseigo"));
    QCOMPARE(feeds.at(1)->label(), QString("aseigo"));
}

void FeedModelTest::decreaseUnreadByFeedIndexByOne()
{
    int index = 1;
    // index 1 = News category
    FeedModel *model = parser.getCategoryModel();
    qDebug() << "label: " << model->at(index)->label();
    model->markFeedAsRead(index);

    QCOMPARE(model->globalUnreadItems(), 1361.0);
    QCOMPARE(model->at(index)->unreadCount(), 999.00);
}

void FeedModelTest::decreaseUnreadByFeedIndexByEight()
{
    int index = 1;
    // index 1 = News category
    FeedModel *model = parser.getCategoryModel();
    qDebug() << "label: " << model->at(index)->label();
    model->markFeedsAsRead(index, 8);

    QCOMPARE(model->globalUnreadItems(), 1354.0);
    QCOMPARE(model->at(index)->unreadCount(), 992.00);
}




// private

void FeedModelTest::openJsonFile(const QString &fileName)
{
    QFile file;
    file.setFileName(QFINDTESTDATA("data/" + fileName));

    int retval;
    retval = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if(retval != 1) {
        qDebug() << "failed to open: " << file.fileName();
    }
    else {
        jsonString.clear();
        jsonString = file.readAll();
        file.close();
        jsonString = jsonString.simplified();
    }
    QVERIFY(retval == true);
}
