#ifndef PARSESUBSCRIPTIONSTEST_H
#define PARSESUBSCRIPTIONSTEST_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"

class ParseSubscriptionsTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit tests
    void categoriesStillFlaggedAsCategories();
    void feedsAreFlaggedAsSubscriptions();
    void verifyFirstCategoryFirstItem();
    void verifySecondCategoryFirstItem();
    void verifySecondCategorySecondItem();
    void verifySecondCategoryThirdItem();
    void verifySecondCategoryFourthItem();
    void verifyThirdCategoryFirstItem();
    void verifyThirdCategorySecondItem();
    void verifyFourthCategoryFirstItem();
    void verifyFourthCategorySecondItem();
    void verifyFourthCategoryThirdItem();
    void verifyFirstFeedWithNoCategory(); // ;
    void verifySecondFeedWithNoCategory(); // ;

private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;
};

#endif
