#ifndef PARSECATEGORIESTEST_H
#define PARSECATEGORIESTEST_H

#include <QtTest/QtTest>
#include "libs/feedlyparser.h"

class ParseCategoriesTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    // unit test
    void testDataShouldHaveFourCategories();
    void globalUnreadCountShouldBeZero();
    void itemsAreFlaggedAsCategories();
    void firstItemHasCorrectIdAndLabel();
    void secondItemHasCorrectIdAndLabel();
    void thirdItemHasCorrectIdAndLabel();
    void fourthItemHasCorrectIdAndLabel();


private:
    void openJsonFile(const QString &fileName);
    QString jsonString;
    FeedlyParser parser;

};

#endif














