#ifndef PARSEPROFILETEST_H
#define PARSEPROFILETEST_H

#include <QtTest/QtTest>

#include "libs/feedlyparser.h"

class ParseProfileTest: public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void cleanupTestCase();

    void init();
    void cleanup();

    // unit tests
    void parseClient();
    void parseEmail();
    void parseFullName();
    void parseGender();
    void parseWordPressConnected();
    void parseEvernoteConnected();
    void parseFamilyName();
    void parseGivenName();
    void parseGoogle();
    void parseId();
    void parseLocale();
    void parsePicture();
    void parsePocketConnected();
    void parseWave();

private:
    void openJsonFile(const QString &fileName);

    QString jsonString;
    FeedlyParser parser;
};


#endif





