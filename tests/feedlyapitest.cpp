#include <QDebug>
#include <QSettings>
#include <QJsonDocument>

#include "feedlyapitest.h"
#include "tests/data/feedlyapidata.h"

// init tests
void FeedlyApiTest::initTestCase()
{
    QSettings settings;
    accessToken = settings.value("access_token").toString();
}

void FeedlyApiTest::cleanupTestCase()
{
}

void FeedlyApiTest::init()
{
    feedly = new FeedlyApi();
    feedly->parseResults(false);
    feedly->setAccessToken(accessToken);
}

void FeedlyApiTest::cleanup()
{
    // print raw json
    /*
    if(qstrcmp(QTest::currentTestFunction(), "beginAuthShouldReturnTrue") != 0) {
        QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
        qDebug() << "json document: " << doc.toJson();
        if(doc.isNull()) {
            qDebug() << "is null";
        }
        if(doc.isEmpty()) {
            qDebug() << "is empty";
        }
        if(doc.isObject()) {
            qDebug() << "is object";
        }
        if(doc.isArray()) {
            qDebug() << "is array";
        }
    }
    */
    delete feedly;
}

// unit tests
void FeedlyApiTest::beginAuthShouldReturnTrue()
{
    QSKIP("tmp disable");
    QVERIFY(feedly->authRequest() == true);
}

void FeedlyApiTest::accessTokenIsNotEmpty()
{
    QVERIFY(accessToken.isEmpty() == false);
}


void FeedlyApiTest::profileRequestShouldReturnTrue()
{
    //feedly->setAccessToken(accessToken);
    //QSKIP("tmp disable");
    QVERIFY(feedly->profileRequest() == true);
}

void FeedlyApiTest::preferencesRequestShouldReturnTrue()
{
    QSKIP("tmp disable");
    QVERIFY(feedly->preferencesRequest() == true);
    QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
    qDebug() << "json document: " << doc.toJson();
}

void FeedlyApiTest::categoriesRequestShouldReturnTrue()
{
    QSKIP("tmp disable");
    QVERIFY(feedly->categoriesRequest() == true);
    QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
    qDebug() << "json document: " << doc.toJson();
}

void FeedlyApiTest::subscriptionsRequestShouldReturnTrue()
{
    QSKIP("tmp disable");
    QVERIFY(feedly->subscriptionsRequest() == true);
    QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
    qDebug() << "json document: " << doc.toJson();
}

void FeedlyApiTest::markersRequestShouldReturnTrue()
{
    QSKIP("tmp disable");
    QVERIFY(feedly->markersRequest() == true);
    QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
    qDebug() << "json document: " << doc.toJson();
}

void FeedlyApiTest::entriesMgetByFromFeedIdShouldReturnTrue()
{
    //QSKIP("tmp disable");
    QVERIFY(feedly->entriesMgetFromFeedId(FeedIdExample) == true);
    QJsonDocument doc = QJsonDocument::fromJson(feedly->printResults().toUtf8());
    qDebug() << "json document: " << doc.toJson();
}






























