import QtQuick 2.0
import Ubuntu.Components 0.1
import QtWebKit 3.0
import "components"

Page {
    id: root
    property string returnCode;
    function loadAuthorization(shtml, surl) {
        mainwindow.loggingFromQml("AuthPage.qml::loadAuthorization", "called")
        //console.log("html: " + shtml)
        webView.loadHtml(shtml, surl, "")
        //webView.url = tempUrl
    }

    Connections { target: feedlyapi; onLoadAuthorizationPage: loadAuthorization(html, url) }

    Item {
        anchors.fill: parent

        WebView {
            id: webView
            anchors.fill: parent
            transformOrigin: Item.TopLeft
            smooth: false
            onNavigationRequested: {
                //mainwindow.loggingFromQml("AuthPage.qml", "url: " + request.url)
                console.log("\n")
                mainwindow.loggingFromQml("AuthPage.qml", "-------------------------\n")
                switch(request.navigationType) {
                case WebView.LinkClickedNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "link clicked navigation")
                    console.log("onNaviationChanged::url: " + request.url)
                    if (/^(https|http):\/\/(localhost)/.test(request.url)) {
                        feedlyapi.parseAuthorizationResponse(request.url)
                    }
                    break;
                case WebView.FormSubmittedNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "form submitted navigation")
                    console.log("onNavigationChanged::url: " + request.url)
                    if (/^(https|http):\/\/(localhost)/.test(request.url)) {
                        feedlyapi.parseAuthorizationResponse(request.url)
                    }
                    else if (/^(https|http):\/\/(cloud.feedly.com\/v3\/auth\/callback)/.test(request.url)) {
                        mainwindow.isBusy = true
                    }
                    break;
                case WebView.BackForwardNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "back forward navigation")
                    break;
                case WebView.ReloadNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "reload navigation")
                    break;
                case WebView.FormResubmittedNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "form resubmitted navigation")
                    break;
                case WebView.OtherNavigation:
                    mainwindow.loggingFromQml("AuthPage.qml", "other navigation")
                    //request.action = WebView.IgnoreRequest
                    console.log("onNavigationChanged::url: " + request.url)
                    break;
                }
            } // onNavigationRequested
            /*
            onUrlChanged: {
                console.log("\n")
                console.log("AuthPage.qml onUrlChanged") //: " + webView.url)
                var resultUrl = webView.url.toString()
                console.log("onUrlChanged resultUrl: " + resultUrl)
                if (/^(https|http):\/\/(localhost)/.test(resultUrl)) {
                //if(resultUrl.indexOf("localhost") != -1) {
                    console.log("onUrlChanged::got localhost: " + resultUrl)
                    if(resultUrl.indexOf("code") != -1) {
                        console.log("onUrlChanged::got code: " + resultUrl)
                        // successful return
                        var startTrim = resultUrl.indexOf("code") + 5;
                        var endTrim = resultUrl.indexOf("&");
                        if(endTrim > startTrim) {
                            returnCode = resultUrl.substring(startTrim, endTrim);
                        }
                        if(returnCode.length > 1) {
                            // set code recieved to true
                            //codeReceived = true;
                            //if(!accessSent) {
                            //    accessSent = true;
                            // call Access function to complete OAuth
                            //    getAccess();
                            //}
                        }
                        console.log("onUrlChanged:: code, returnCode: " + returnCode)

                    }
                }
                if(resultUrl.indexOf("invalid_request") != -1) {
                    console.log("onUrlChanged::The request was malformed or missing parameters");
                }
                else if(resultUrl.indexOf("unauthorized_client") != -1) {
                    console.log("onUrlChanged::The client is not authorized");
                }
                else if(resultUrl.indexOf("access_denied") != -1) {
                    console.log("onUrlChanged::The user denied the request for authorization");
                }
                else if(resultUrl.indexOf("unsupported_response_type") != -1) {
                    console.log("onUrlChanged::Meetup doesn't support the provided response_type");
                }
                else if(resultUrl.indexOf("invalid_grant") != -1) {
                    console.log("onUrlChanged::The provided code was invalid");
                }
                else if(resultUrl.indexOf("unsupported_response_type") != -1) {
                    console.log("onUrlChanged::Meetup does not support the provided grant type");
                }
                else {
                    // other
                    console.log("onUrlChanged::Other data " + resultUrl);
                }
            }
            */
            /*
            onLoadingChanged: {
                mainwindow.loggingFromQml("AuthPage.qml", "LOADREQUEST --------------")
                console.log("AuthPage.qml    loadRequest.url " + loadRequest.url)
//                mainwindow.loggingFromQml("AuthPage.qml", "loadRequest.url: " + loadRequest.url)
                mainwindow.loggingFromQml("AuthPage.qml", "loadRequest.status: " + loadRequest.status)
                mainwindow.loggingFromQml("AuthPage.qml", "loadRequest.errorString: " + loadRequest.errorString)
                mainwindow.loggingFromQml("AuthPage.qml", "loadRequest.errorCode: " + loadRequest.errorCode)
                mainwindow.loggingFromQml("AuthPage.qml", "loadRequest.errorDomain: " + loadRequest.errorDomain)
            }
            */
        } // WebView
    } // item

    tools: MainPageTabTools {
        id: toolBar
        //anchors.fill: parent
        locked: true
    }

} // page






/*
                    if (/^(https|http):\/\/(sandbox.feedly.com)/.test(request.url)) {
                        request.action = WebView.IgnoreRequest
                        break;
                    }
                    else if (/^(https|http):\/\/(localhost)/.test(request.url)) {
                        request.action = WebView.IgnoreRequest
                        break;
                    }
                    else if (/^(https|http):\/\/(feedly.com)/.test(request.url)) {
                        request.action = WebView.IgnoreRequest
                        break;
                    }
                    //if (/^(https|http):\/\/(accounts.google.com)/.test(request.url)) {
                    //    request.action = WebView.AcceptRequest
                    //}
                    //else {
                    //    request.action = WebView.IgnoreRequest
                    // }
                    //request.action = WebView.AcceptRequest
                    */

























