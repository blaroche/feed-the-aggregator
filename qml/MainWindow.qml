import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Layouts 0.1
import Ubuntu.Components.Popups 0.1

import "components"

MainView {
    id: mainView
    objectName: "mainView"
    applicationName: mainwindow.getApplicationName()
    //automaticOrientation: true
    headerColor: UbuntuColors.orange

    width: units.gu(43)
    height: units.gu(68)
    //Layouts { id: mainLayout; anchors.fill: parent }

    function loadEntryViewPage(entryId, content) {
        console.log("MainWindow.qml::loadEntryVeiwPage called")
        pageStack.push(entryViewPage)
        entryViewPage.loadPage(entryId, content)
    }
    function loadAuthPage() {
        pageStack.push(authPage)
    }
    function loadMainPage() {
        pageStack.push(mainPage)
    }
    function refresh() {
        mainPage.clear()
        pageStack.clear()
        pageStack.push(mainPage)
        mainwindow.refresh()
    }

    Connections { target: mainwindow; onLoadAuthPage: loadAuthPage() }
    Connections { target: mainwindow; onLoadMainPage: loadMainPage() }

    PageStack {
        id: pageStack
        anchors.fill: parent

        ActivityIndicator {
            id: activity
            width: units.gu(7)
            height: units.gu(7)
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            running: mainwindow.isBusy
            z: 20
        }

        NavigationHeader {
            id: navHeader
            x: 0
            y: 0
            width: parent.width
            height: units.gu(7)
            visible: (pageStack.currentPage == mainPage) ? true : false
            z: 5
        }

        /*
        EntryHeader {
            id: entryHeader
            x: 0
            y: 0
            width: parent.width
            height: units.gu(7)
            visible: (pageStack.currentPage == entryViewPage) ? true : false
            z: 5
        }
        */
        StartupPage {
            id: startupPage
            visible: false
            anchors.fill: parent
        }

        MainPage {
            id: mainPage
            visible: false
            anchors {
                left: parent.left
                right: parent.right
                top: navHeader.bottom
            }
        }

        EntryViewPage {
            id: entryViewPage
            visible: false
            anchors.fill: parent
            //anchors {
            //    left: parent.left
            //    right: parent.right
            //    top: entryHeader.bottom
            //}
        }

        EntryWebView {
            id: entryWebViewPage
            visible: false
            anchors.fill: parent;
        }

        AuthPage {
            id: authPage
            visible: false
            anchors.fill: parent
        }


        Component.onCompleted: {
            pageStack.push(startupPage)
        }
    } // pageStack
}  // mainview
