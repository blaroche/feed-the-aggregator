import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems
import "components"


Page {
    id: root

    property bool showList: false
    //property int categoryIndex: 0
    //property string feedId: ""

    function loadEntries() {
        showList = true
        loaderEntries.source = "qrc:/qml/components/EntriesListView.qml"
        toolBar.markAllReadVisible = true
    }
    function clear() {
        console.log("Mainpage.qml::clear called")
        toolBar.markAllReadVisible = false
        loaderEntries.source = ""
        toolBar.opened = false
    }
    function markFeedAsRead() {
        toolBar.opened = false
        mainwindow.markFeedAsRead()
    }

    Connections { target: mainwindow; onLoadEntriesList: loadEntries() }

    Item {
          id: backgroundRect
          anchors.fill: parent
          anchors.margins: units.gu(1)
          //radius: units.gu(0.2)
          smooth: true
          //color: UbuntuColors.warmGrey

          Loader {
              id: loaderEntries
              anchors.fill: parent
          }

    } // item

    tools: MainPageTabTools {
        id: toolBar
    }
} // page
