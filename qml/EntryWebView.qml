import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems
import QtWebKit 3.0
import "components"

Page {
    id: root

    function loadWebView(url) {
        //console.log("url: " + url)
        webView.url = url
    }
    function goBack() {
        console.log("goBack called")
        webView.goBack()
    }

    Item {
        anchors.fill: parent

        WebView {
            id:  webView
            anchors.fill: parent


        } // webview
    } // item

    tools: EntryWebViewTools {
        id: toolBar
    }

} // page






