import QtQuick 2.0
import QtGraphicalEffects 1.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems


Item {
    id: root
    smooth: true

    function openBrowser() {
        //console.log("open browser")
        Qt.openUrlExternally(mainwindow.getEntryUrl())
    }
    function openWebView() {
        //console.log("open web view")
        entryWebViewPage.loadWebView(mainwindow.getEntryUrl())
        pageStack.push(entryWebViewPage)
    }

    //Rectangle {
    UbuntuShape {
        id: headerBackgroundRect
        anchors.fill: parent
        anchors.margins: units.gu(1)
        radius: units.gu(0.6)
        smooth: true
        color: UbuntuColors.orange

        Item {
            id: navItem
            anchors.fill: headerBackgroundRect
            visible: true
            anchors {
                margins: units.gu(1)
            }
        } // item
        Label {
            id: tempLabel
            //text: "options coming to this space soon"
            text: ""
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            //anchors.left: parent.left
            color: "black"
        } // label
        Image {
            id: externalBrowerImage
            anchors.right: headerBackgroundRect.right
            width: units.gu(5)
            height: units.gu(5)
            source: "qrc:/img/video-player-7.png"
            MouseArea {
                anchors.fill: parent
                onClicked: openBrowser()
            } // mousearea

        }

        Image {
            id: loadWebViewImage
            anchors.right: externalBrowerImage.left
            width: units.gu(5)
            height: units.gu(5)
            source: "qrc:/img/alarm-7.png"
            MouseArea {
                anchors.fill: parent
                onClicked: openWebView()
            } // mousearea

        }

    } // ubuntushape
} // item

