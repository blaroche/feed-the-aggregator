import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems
import Ubuntu.Components.Popups 0.1

ListView {
    id: listView
    visible: true
    clip: true
    delegate: listDelegate
    //model: ExampleModel {}
    model: categoryModel
    focus: true

    function getEntries(id) {
        console.log("NavigationListView.qml getEntries() entered")
        console.log("NavigationListView.qml feedid: " + id)
        mainwindow.getEntries(id)
        navPopover.hide()
        //mainPage.categoryIndex = index
        //mainwindow.categoryIndex = index
        //mainPage.feedId = id
    }

    ActivityIndicator {
        id: activity
        width: units.gu(7)
        height: units.gu(7)
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        running: mainwindow.isBusy
        z: 20
    }

    Component { id: listDelegate
        ListItems.Standard {
            //ListItems.Empty {
            showDivider: false
            Item {
                id: delegateItem
                anchors.fill: parent
                //width: listView.width
                //height: units.gu(10) //5// 20
                state: "defaultRow"

                Rectangle {
                    //UbuntuShape {
                    id: expandChildrenRect
                    height: parent.height
                    width: units.gu(3.25)
                    radius: 4
                    border.color: "lightgrey"
                    //anchors.left: (parent.left+15)
                    Image {
                        id: expandChildrenImage
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        verticalAlignment: Image.AlignVCenter
                        horizontalAlignment: Image.AlignHCenter
                        //source: "qrc:/img/downward-arrow-7.png"
                        MouseArea  {
                            id: expandChildrenMouseArea
                            anchors.fill:  parent
                            onClicked: (isOpened) ? categoryModel.closeRow(index) : categoryModel.openRow(index)
                        }
                    } // image
                } // rect

                UbuntuShape {
                    id: labelRect
                    anchors.margins: 1
                    anchors.left: expandChildrenRect.right
                    anchors.right: parent.right
                    height: parent.height //-3

                    Label {
                        id: labelText
                        anchors.leftMargin: 3
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        text: label
                        verticalAlignment: Text.AlignVCenter
                        fontSize: "small"
                    } // label

                    Label {
                        id: unreadText
                        anchors.leftMargin: 10
                        anchors.left: labelText.right
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        text: "(" + unreadCount + ")"
                        verticalAlignment: Text.AlignVCenter
                        fontSize: "small"
                    } // label

                    MouseArea {
                        anchors.fill: parent
                        onClicked: if(isSubscription) getEntries(id)
                   }
                } // ubuntuShape

                states : [
                    State {
                        name: "defaultRow"
                        when: !hasUnread && isCategory
                        PropertyChanges {
                            target: expandChildrenImage
                            visible: false
                            //width: 0
                            //height: 0
                        }
                        PropertyChanges {
                            target: expandChildrenMouseArea
                            z: -100
                        }
                        PropertyChanges {
                            target: unreadText
                            visible: false
                        }
                    },
                    State {
                        name: "categoryIsExpaned"
                        //when: (hasChildren) && (isOpened)
                        when: isOpened && isCategory
                        //when: categoryModel.at(index).isOpened
                        PropertyChanges {
                            target: expandChildrenImage
                            visible: true
                            //source:  "qrc:/img/upward-arrow-7.png"
                            source: "qrc:/img/downward-arrow-7.png"
                            rotation: 180
                        }
                    },
                    State {
                        name: "categoryCanExpand"
                        when: (hasChildren) && (!isOpened) && (hasUnread) && isCategory
                        //when: (hasChildren) && (!isOpened) && isCategory
                        PropertyChanges {
                            target: expandChildrenImage
                            visible: true
                            source: "qrc:/img/downward-arrow-7.png"
                        }
                    },
                    State {
                        name: "subscriptionRow"
                        when: isSubscription
                        PropertyChanges {
                            target: expandChildrenImage
                            visible: false
                        }
                        PropertyChanges {
                            target: expandChildrenRect
                            width: 50
                            border.color: "#FFFFFF"
                            //color: "yellow"
                        }
                        PropertyChanges {
                            target: labelRect
                            color: "#e0e0e0" //UbuntuColors.warmGrey
                        }
                    }
                ] // states
            } // Item
        } // ListItems
    } // Component
} //ListView









