import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

ToolbarItems {
    id: toolBarItems

    function openBrowser() {
        //console.log("open browser")
        Qt.openUrlExternally(mainwindow.getEntryUrl())
    }
    function openWebView() {
        //console.log("open web view")
        entryWebViewPage.loadWebView(mainwindow.getEntryUrl())
        pageStack.push(entryWebViewPage)
    }

    ToolbarButton {
        text: i18n.tr("Webview")
        iconSource: "qrc:/img/map-pin-7.png"
        onTriggered: openWebView()
    }

    ToolbarButton {
        text: i18n.tr("Browser")
        iconSource: "qrc:/img/book-7.png"
        onTriggered: openBrowser()
    }
}
