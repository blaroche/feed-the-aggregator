import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

ToolbarItems {
    id: toolBarItems

    ToolbarButton {
        text: i18n.tr("back")
        iconSource: "qrc:/img/backward-7.png"
        onTriggered: entryWebViewPage.goBack()
    }

    /*
    ToolbarButton {
        text: i18n.tr("reload")
        iconSource: "qrc:/img/box-down-7.png"
        //onTriggered: //mainView.refresh()
    }
    */
}
