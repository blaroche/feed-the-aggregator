import QtQuick 2.0
import QtGraphicalEffects 1.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems


Item {
    id: root
    smooth: true
    z: 10
    property string applicationTitle: "Feed the Aggregator"
    //property string categoryLabel: ""

    function refresh() {
    }

    //UbuntuShape {
    Rectangle {
        id: headerBackgroundRect
        anchors.fill: parent
        smooth: true
        color: UbuntuColors.orange

        Item {
            id: navItem
            anchors.fill: headerBackgroundRect
            visible: true
            anchors {
                margins: units.gu(1)
            }

            Image{
                id: navOpenIcon
                anchors {
                    left: navItem.left
                    leftMargin: units.gu(1)
                    verticalCenter: navItem.verticalCenter
                }
                property bool isOpen: false
                //source: "qrc:/img/downward-arrow-7.png"
                source: "qrc:/img/list-star-7.png"
                fillMode: Image.PreserveAspectFit
                height: units.gu(5)
                width: units.gu(5)

                MouseArea {
                    id: navMouseArea
                    anchors.fill: parent
                    onClicked:  PopupUtils.open(navPopupList, navOpenIcon)
                } // mousearea

            } // image

            Label {
                id: applicationNameLabel
                text: applicationTitle
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                //anchors.left: parent.left
                color: "black"
                fontSize: "large"
            } // label

            Label {
                id: globalUnreadItemLabel
                //text: if(category.globalUnreadItems > 0) "("+categoryModel.globalUnreadItems+")"
                text: "("+categoryModel.globalUnreadItems+")"
                anchors.left: applicationNameLabel.right
                anchors.leftMargin: units.gu(2)
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                color: "black"
                fontSize: "medium"

            }


            Component {
                id: navPopupList
                Popover {
                    id: navPopover
                    contentWidth: headerBackgroundRect.width - units.gu(5)
                    contentHeight: navListView.height
                    autoClose: true

                    Column {
                        id: navListColumn
                        anchors {
                            top: parent.top
                            left: parent.left
                            right: parent.right
                        }
                        height: mainView.height - (navItem.y + navItem.height + units.gu(5))

                        //height: parent.height
                        //height: mainView.height - (navItem.y + navItem.height )
                        //height: navPopover.contentHeight

                        NavigationListView {
                            id: navListView
                            height: parent.height - 50
                            width: parent.width

                        } // navigationlistview
                    } // column
                } // popover
            } // componet
        } // item
    } // rect
} // item
