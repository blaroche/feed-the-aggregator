import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems

ListView {
    id:  entriesListView
    visible: true
    clip: true
    delegate: listDelegate
    //model: ExampleModel{}
    model: entriesModel
    focus: true
    //property int categoryIndex: 0

    //function entryClicked(entryId, content, unread, index) {
    function entryClicked(entryId, content) {
        mainwindow.loggingFromQml("EntriesListView.qml:: entryClicked", "called")
        mainwindow.markEntryAsRead()
        mainView.loadEntryViewPage(entryId, content)
    }

    Component {
        id: listDelegate

        ListItems.Standard {
            showDivider: false
            height: units.gu(5)

            Item {
                id: delegateItem
                anchors.fill: parent

                UbuntuShape {
                    id: labelRect
                    anchors.fill: parent
                    anchors.margins: 2

                    Label {
                        id: labelText
                        color: (unread) ? "black" : "grey"
                        anchors.leftMargin: 3
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.rightMargin: 3
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        text: title
                        verticalAlignment: Text.AlignVCenter
                        fontSize: "small"
                        font.bold: unread
                        //wrapMode: Text.WrapAnywhere
                        wrapMode: Text.Wrap
                    } // label
                    MouseArea {
                      anchors.fill: parent
                      //onClicked: entryClicked(entryId, content, unread, index)
                      onClicked: entryClicked(entryId, content)
                    }

                } // ubuntushape
            } // item
        } // listitems
    } // component
} // listviewl


