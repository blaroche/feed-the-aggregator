import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

ToolbarItems {
    id: toolBarItems

    back.visible: false
    property bool markAllReadVisible: false

    ToolbarButton {
        text: i18n.tr("mark all read")
        id: markAllReadToolbutton
        visible: markAllReadVisible
        iconSource: "qrc:/img/back-arrow-7.png"
        onTriggered: mainPage.markFeedAsRead()
    }

    ToolbarButton {
        text: i18n.tr("refresh")
        iconSource: "qrc:/img/box-down-7.png"
        onTriggered: mainView.refresh()
    }
    ToolbarButton {
        text: i18n.tr("about")
        iconSource: "qrc:/img/man-7.png"
        onTriggered: {
            PopupUtils.open(aboutComponent)
        }
    }


    // about popup
    Component {
        id: aboutComponent
        //Popover {
        DefaultSheet {
            id: aboutSheet
            anchors.fill: parent

            Flickable {
                anchors.fill: parent
                contentHeight: aboutTextArea.paintedHeight
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.DragAndOvershootBounds
                clip: true

                Label {
                    id: aboutTextArea
                    text: mainwindow.getAboutContents()
                    fontSize: "small"
                    anchors{
                        fill: parent
                        margins: units.gu(1.5)
                    }
                } // Label
            } // Flickable
        } // DefaultSheet
    } // Component

}



























