import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
//Header {
        id: headerRect
        color: "green"
        property int fontWeight: Font.Light
        property string fontSize: "x-large"
        property string subFontSize: "large"
        property color textColor: "black"
        property real textLeftMargin: units.gu(2)
        visible: true

        property string title: "test"
        property string subTitle: "frrt"
        //property bool linkState: linkOpen

        Item {
            id: headerContents
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            height: headerRect.contentHeight
            visible: true

            Label {
                id: headerLabel
                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                    leftMargin: headerRect.textLeftMargin
                }
                text: headerRect.title
                font.weight: headerRect.fontWeight
                fontSize: headerRect.fontSize
                color: headerRect.textColor
            }

            Label {
                id: headerSubLabel
                anchors {
                    left: headerLabel.right
                    leftMargin: units.gu(1.35)
                    right: headerArrow.left
                    rightMargin: units.gu(1.35)
                    bottom: headerLabel.bottom
                    bottomMargin: units.gu(0.35)
                }
                text: headerRect.subTitle
                font.weight: headerRect.fontWeight
                fontSize: headerRect.subFontSize
                elide: Text.ElideRight
                color: headerRect.textColor
            }
            Image {
                id: headerArrow
                anchors {
                    right: parent.right
                    rightMargin: units.gu(1.35)
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: units.gu(0.2)
                }
                visible: true
                source: "qrc:/img/downward-arrow-7.png"
                rotation: headerRect.linkState ? 180 : 0
            }
        }

}
