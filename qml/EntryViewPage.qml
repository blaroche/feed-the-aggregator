import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItems
//import QtWebKit 3.0
import "components"


Page {
    id: root

    function loadPage(entryId, content) {
        console.log("EntyViewPage.qml loadPage entered")
        contentView.text = content
    }
    //function clear() {
    //    console.log("entryViewPage clear")
    //    pageStack.clear()
    //}

    Item {
        id: pageItem
        anchors.fill: parent

        Flickable {
            id: flickable
            anchors.fill: parent
            contentHeight: contentView.paintedHeight
            flickableDirection: Flickable.VerticalFlick
            boundsBehavior: Flickable.DragAndOvershootBounds
            clip: true

            Label {
                id: contentView
                anchors.fill: parent
                anchors.margins: units.gu(2)
                wrapMode: Text.Wrap
                fontSize: "medium"
                color: "black"
            }
        } // flickable
    } // item

    tools: EntryViewTools {
        id: toolBar
        //anchors.fill: parent
        back.visible: true
    }
} // page
