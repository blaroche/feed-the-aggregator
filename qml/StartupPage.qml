import QtQuick 2.0
import Ubuntu.Components 0.1
import "components"


Page {
    id: root
    //title: "startup"
    anchors.fill: parent

    ActivityIndicator {
        id: activity
        width: 60
        height: 60
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        running: mainwindow.isBusy
    }

    /*
    tools: MainPageTabTools {
        id: toolBar
        anchors.fill: parent
    }
    */
}
