#include <QDebug>
#include <QHash>
#include <QByteArray>
#include <QStringList>

#include "categorymodel.h"
#include "subscriptionmodel.h"


Category::Category()
{
    mUnreadCount = 0;
    mUpdated = 0;
    mIsOpened = false;
    mSubscriptionModel = NULL;
}

Category::~Category()
{
    if(mSubscriptionModel)
        delete mSubscriptionModel;
}

bool Category::isEmpty() const
{
    return (mId.isEmpty() && mLabel.isEmpty());
}

// setters
void Category::setId(const QString &id)
{
    mId = id;
}

void Category::setLabel(const QString &label)
{
    mLabel = label;
}

void Category::setUnreadCount(const double &count)
{
    mUnreadCount = count;
}

void Category::setUpdated(const double &updated)
{
    mUpdated = updated;
}

void Category::setIsOpened(const bool &opened)
{
    mIsOpened = opened;
}

void Category::setSubscriptionModel(SubscriptionModel *model)
{
    mSubscriptionModel = model;
}

void Category::appendSubscriptionModel(Subscription *subscription)
{
    if(mSubscriptionModel == NULL)
        mSubscriptionModel = new SubscriptionModel();

    mSubscriptionModel->appendRow(subscription);
}

// getters
QString Category::id() const
{
    return mId;
}

QString Category::label() const
{
    return mLabel;
}

double Category::unreadCount() const
{
    return mUnreadCount;
}

double Category::updated() const
{
    return mUpdated;
}

bool Category::isOpened() const
{
    return mIsOpened;
}

SubscriptionModel* Category::subscriptionModel() const
{
    return mSubscriptionModel;
}




// ************** model *********************
CategoryModel::CategoryModel() :
    QAbstractListModel()
{
    mRoleNames = roleNames();
}

CategoryModel::~CategoryModel()
{
    foreach(Category *cat, mCategories) {
        delete cat;
        mCategories.clear();
    }
}

QList<Category* > CategoryModel::categories() const
{
    return mCategories;
}

bool CategoryModel::isEmpty() const
{
    return mCategories.isEmpty();
}

void CategoryModel::clear()
{
    if(!mCategories.isEmpty())
        mCategories.clear();
}

QHash<int, QByteArray> CategoryModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Id] = "id";
    roles[Label] = "label";
    roles[OpenRow] = "openRow";
    roles[CloseRow] = "closeRow";
    roles[IsOpened] = "isOpened";
    roles[HasChildren] = "hasChildren";
    return roles;
}

Category* CategoryModel::at(const int index)
{
    Category *cat = mCategories.at(index);
    return cat;
}

Category* CategoryModel::findCategoryById(const QString &id)
{
    foreach(Category *cat, mCategories) {
        if(cat->id() == id)
            return cat;
    }
    return NULL;
}

void CategoryModel::updateCategoryById(const QString &id, const double &count, const double &updated)
{
    int i;
    if(id.isEmpty())
        return;

    for(i = 0; i < mCategories.count(); i++) {
        if(mCategories[i]->id() == id) {
            mCategories[i]->setUnreadCount(count);
            mCategories[i]->setUpdated(updated);
            return;
        }
    }
}

void CategoryModel::insertRow(int row, Category *category)
{
    beginInsertRows(QModelIndex(), row, row);
    //connect(item, SIGNAL(dataChanged()), SLOT(handleItemChange()));
    mCategories.insert(row, category);
    endInsertRows();
}

void CategoryModel::appendRow(Category *category)
{
    mCategories.append(category);
}

int CategoryModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mCategories.count();
}

QVariant CategoryModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row() < 0 || index.row() > (mCategories.count() - 1))
        return QVariant();

    switch(role) {
    case Id:
        return mCategories.at(index.row())->id();
        break;
    case Label:
        return mCategories.at(index.row())->label();
        break;
    //case OpenRow:
        //openRow(index.row());
    //    test();
    //    break;
    //case CloseRow:
    //    test();
        //closeRow(index.row());
    //    break;
    case IsOpened:
        return mCategories.at(index.row())->isOpened();
        break;
    case HasChildren:
        return mCategories.at(index.row())->hasChildren();
        break;
    default:
        return QVariant();

    };
}

// public slots
void CategoryModel::openRow(int index)
{
    qDebug() << "openRow called: " << index;

    if(index > (mCategories.count() -1))
        return;
    if(mCategories[index]->isOpened())
        return;
    mCategories.at(index)->setIsOpened(true);
//    emit dataChanged(modelIndex, modelIndex);
    //int i = index + 1;

    //foreach(Subscription *sub, mCategories.at(index)->subscriptionModel()) {
    for(int j = 0; j < mCategories.at(index)->subscriptionModel()->rowCount(); j++ ) {
        //Subscription *sub = mCategories.at(index)->subscriptionModel()->at(j);
        //mCategories.insert(i++, sub);
        //qDebug() << "sub title: " << sub->title();
    }
}

void CategoryModel::closeRow(int index)
{
    qDebug() << "closeRow called: " << index;
    if(index > (mCategories.count() -1))
        return;

}






















