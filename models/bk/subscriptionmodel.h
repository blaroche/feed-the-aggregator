#ifndef SUBSCRIPTIONMODEL_H
#define SUBSCRIPTIONMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QString>
#include <QList>

class Subscription
{
public:
    Subscription();
    ~Subscription();
    inline bool hasChildren() { return false; }
    bool isEmpty() const;

    void setId(const QString &id);
    void setTitle(const QString &title);
    void setSortId(const QString &sortId);
    void setUpdated(const double &updated);
    void setWebsite(const QString &website);
    void setUnreadCount(const double &count);

    QString id() const;
    QString title() const;
    QString sortId() const;
    double updated() const;
    QString website() const;
    double unreadCount() const;
private:
    Q_DISABLE_COPY(Subscription)
    QString mId;
    QString mTitle;
    QString mSortId;
    double mUpdated;
    QString mWebsite;
    double mUnreadCount;
};



// ****************************************
class SubscriptionModel : public QAbstractListModel
{
    //Q_OBJECT
public:
    enum Roles {
        Id = Qt::UserRole + 1,
        Title,
        SortId,
        Updated,
        WebSite
    };

    SubscriptionModel();
    ~SubscriptionModel();

    QList<Subscription* > subscriptions() const;
    bool isEmpty() const;
    void clear();
    QHash<int, QByteArray> roleNames() const;
    Subscription* at(const int index);
    void updateUnreadCountsById(const QString &id,
                                const double &count,
                                const double &updated);
    void updateSubscritionById(const Subscription &subscription);
//    int getIndexById(const QString &id);
//    void updateSubscriptionById(const QString &id);

    void appendRow(Subscription *subscription);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
private:
    Q_DISABLE_COPY(SubscriptionModel)
    QHash<int, QByteArray> mRoleNames;
    QList<Subscription* > mSubscriptions;
};

#endif // SUBSCRIPTIONMODEL_H






















