#include <QDebug>
#include <QStringList>

#include "subscriptionmodel.h"

Subscription::Subscription()
{
    mUpdated = 0;
    mUnreadCount = 0;
}

Subscription::~Subscription()
{
}

bool Subscription::isEmpty() const
{
    return (mId.isEmpty() && mTitle.isEmpty() && mSortId.isEmpty() && mWebsite.isEmpty());
}

// setters
void Subscription::setId(const QString &id)
{
    mId = id;
}

void Subscription::setTitle(const QString &title)
{
    mTitle = title;
}

void Subscription::setSortId(const QString &sortId)
{
    mSortId = sortId;
}

void Subscription::setUpdated(const double &updated)
{
    mUpdated = updated;
}

void Subscription::setWebsite(const QString &website)
{
    mWebsite = website;
}

void Subscription::setUnreadCount(const double &count)
{
    mUnreadCount = count;
}

// getters
QString Subscription::id() const
{
    return mId;
}

QString Subscription::title() const
{
    return mTitle;
}

QString Subscription::sortId() const
{
    return mSortId;
}

double Subscription::updated() const
{
    return mUpdated;
}

QString Subscription::website() const
{
    return mWebsite;
}

double Subscription::unreadCount() const
{
    return mUnreadCount;
}



// ************* model *****************88


SubscriptionModel::SubscriptionModel() :
    QAbstractListModel()
{
    mRoleNames = roleNames();
}

SubscriptionModel::~SubscriptionModel()
{
    foreach(Subscription *sub, mSubscriptions) {
        delete sub;
        mSubscriptions.clear();
    }
}

QList<Subscription* > SubscriptionModel::subscriptions() const
{
    return mSubscriptions;
}

bool SubscriptionModel::isEmpty() const
{
    return mSubscriptions.isEmpty();
}

void SubscriptionModel::clear()
{
    if(!mSubscriptions.isEmpty())
        mSubscriptions.clear();
}

QHash<int, QByteArray> SubscriptionModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Id] = "id";
    roles[Title] = "title";
    roles[SortId] = "sortid";
    roles[Updated] = "updated";
    roles[WebSite] = "website";
    return roles;
}

Subscription* SubscriptionModel::at(const int index)
{
    Subscription *sub = mSubscriptions.at(index);
    return sub;
}

void SubscriptionModel::updateUnreadCountsById(const QString &id, const double &count, const double &updated)
{
    int i;
    if(id.isEmpty())
        return;

    for(i = 0; i < mSubscriptions.count(); i++) {
        if(mSubscriptions[i]->id() == id) {
            mSubscriptions[i]->setUnreadCount(count);
            mSubscriptions[i]->setUpdated(updated);
            return;
        }
    }
}

void SubscriptionModel::updateSubscritionById(const Subscription &subscription)
{
    int i = 0;

    for(i = 0; i < mSubscriptions.count(); i++) {
        if(mSubscriptions[i]->id() == subscription.id()) {
            mSubscriptions[i]->setUnreadCount(subscription.unreadCount());
            mSubscriptions[i]->setUpdated(subscription.updated());
        }

    }
}

void SubscriptionModel::appendRow(Subscription *subscription)
{
    mSubscriptions.append(subscription);
}

int SubscriptionModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mSubscriptions.count();
}

QVariant SubscriptionModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row() < 0 || index.row() > mSubscriptions.count())
        return QVariant();

    if(role == Id)
        return mSubscriptions.at(index.row())->id();
    else if(role == Title)
        return mSubscriptions.at(index.row())->title();
    else if(role == Qt::DisplayRole)
        qDebug() << "display role";

    return QVariant();
}

























