#ifndef PROFILEMODEL_H
#define PROFILEMODEL_H

#include <QObject>
#include <QString>

class ProfileModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    Q_PROPERTY(QString fullName READ fullName WRITE setFullName NOTIFY fullNameChanged)
    Q_PROPERTY(QString givenName READ givenName WRITE setGivenName NOTIFY givenNameChanged)
    Q_PROPERTY(QString familyName READ familyName WRITE setFamilyName NOTIFY familyNameChanged)
    Q_PROPERTY(QString picture READ picture WRITE setPicture NOTIFY pictureChanged)
    Q_PROPERTY(QString gender READ gender WRITE setGender NOTIFY genderChanged)
    Q_PROPERTY(QString locale READ locale WRITE setLocale NOTIFY localeChanged)
    Q_PROPERTY(QString google READ google WRITE setGoogle NOTIFY googleChanged)
    Q_PROPERTY(QString twitter READ twitter WRITE setTwitter NOTIFY twitterChanged)
    Q_PROPERTY(QString facebook READ facebook WRITE setFacebook NOTIFY facebookChanged)
    Q_PROPERTY(QString wave READ wave WRITE setWave NOTIFY waveChanged)
    Q_PROPERTY(QString client READ client WRITE setClient NOTIFY clientChanged)
    Q_PROPERTY(bool evernoteConnected READ evernoteConnected WRITE setEvernoteConnected NOTIFY evernoteConnectedChanged)
    Q_PROPERTY(bool pocketConnected READ pocketConnected WRITE setPocketConnected NOTIFY pocketConnectedChanged)
    Q_PROPERTY(bool wordPressConnected READ wordPressConnected WRITE setWordPressConnected NOTIFY wordPressConnectedChanged)
public:
    ProfileModel();

    void setId(const QString &id);
    void setEmail(const QString &email);
    void setFullName(const QString &name);
    void setGivenName(const QString &givenName);
    void setFamilyName(const QString &familyName);
    void setPicture(const QString &picture);
    void setGender(const QString &gender);
    void setLocale(const QString &locale);
    void setGoogle(const QString &google);
    void setTwitter(const QString &twitter);
    void setFacebook(const QString &facebook);
    void setWave(const QString &wave);
    void setClient(const QString &client);
    void setEvernoteConnected(const bool &isConnected);
    void setPocketConnected(const bool &isConnected);
    void setWordPressConnected(const bool &isConnected);

    const QString& id() const;
    const QString& email() const;
    const QString& fullName() const;
    const QString& givenName() const;
    const QString& familyName() const;
    const QString& picture() const;
    const QString& gender() const;
    const QString& locale() const;
    const QString& google() const;
    const QString& twitter() const;
    const QString& facebook() const;
    const QString& wave() const;
    const QString& client() const;
    const bool& evernoteConnected() const;
    const bool& pocketConnected() const;
    const bool& wordPressConnected() const;

signals:
    void idChanged();
    void emailChanged();
    void fullNameChanged();
    void givenNameChanged();
    void familyNameChanged();
    void pictureChanged();
    void genderChanged();
    void localeChanged();
    void googleChanged();
    void twitterChanged();
    void facebookChanged();
    void waveChanged();
    void clientChanged();
    void evernoteConnectedChanged();
    void pocketConnectedChanged();
    void wordPressConnectedChanged();

private:
    QString mId;
    QString mEmail;
    QString mFullName;
    QString mGivenName;
    QString mFamilyName;
    QString mPicture;
    QString mGender;
    QString mLocale;
    QString mGoogle;
    QString mTwitter;
    QString mFacebook;
    QString mWave;
    QString mClient;
    bool mEvernoteConnected;
    bool mPocketConnected;
    bool mWordPressConnected;
};

#endif // PROFILEMODEL_H





