#include <QDebug>
#include <QHash>
#include <QByteArray>
#include <QStringList>

#include "entriesmodel.h"

Entry::Entry()
{
    mPublished = 0;
    mUpdated = 0;
    mCrawled = 0;
    mRecrawled = 0;
    mEngagement = 0;
    mUnread = true;
}

Entry::~Entry()
{
}


// setters
void Entry::setEntryId(const QString &entryId)
{
    mEntryId = entryId;
}

void Entry::setCategoryId(const QString &id)
{
    mCategoryId = id;
}

void Entry::setCategoryLabel(const QString &label)
{
    mCategoryLabel = label;
}

void Entry::setOriginHtmlUrl(const QString &url)
{
    mOriginHtmlUrl = url;
}

void Entry::setOriginId(const QString &id)
{
    mOriginId = id;
}

void Entry::setOriginTitle(const QString &title)
{
    mOriginTitle = title;
}

void Entry::setStreamId(const QString &id)
{
    mStreamId = id;
}

void Entry::setTitle(const QString &title)
{
    mTitle= title;
}

void Entry::setAuthor(const QString &author)
{
    mAuthor = author;
}

void Entry::setContent(const QString &content)
{
    mContent.setHtml(content);
//    mContent = content;
}

void Entry::setContentDirection(const QString &direction)
{
    mContentDirection = direction;
}

void Entry::setAlternativeLink(const QString &link)
{
    mAlternativeLink = link;
}

void Entry::setAlternativeType(const QString &type)
{
    mAlternativeType = type;
}

void Entry::setCanonicalLink(const QString &link)
{
    mCanonicalLink = link;
}

void Entry::setFingerprint(const QString &value)
{
    mFingerprint = value;
}

void Entry::setSid(const QString &id)
{
    mSid = id;
}

void Entry::setPulished(const double &timestamp)
{
    mPublished = timestamp;
}

void Entry::setUpdated(const double &timestamp)
{
    mUpdated = timestamp;
}

void Entry::setCrawled(const double &timestamp)
{
    mCrawled = timestamp;
}

void Entry::setRecrawled(const double &timestamp)
{
    mRecrawled = timestamp;
}

void Entry::setEngagement(const int &value)
{
    mEngagement = value;
}

void Entry::setUnread(const bool &trueFalse)
{
    mUnread = trueFalse;
}


// getters
QString Entry::entryId() const
{
    return mEntryId;
}

QString Entry::categoryId() const
{
    return mCategoryId;
}

QString Entry::categoryLabel() const
{
    return mCategoryLabel;
}

QString Entry::originHtmlUrl() const
{
    return mOriginHtmlUrl;
}

QString Entry::originId() const
{
    return mOriginId;
}

QString Entry::originTitle() const
{
    return mOriginTitle;
}

QString Entry::streamId() const
{
    return mStreamId;
}

QString Entry::title() const
{
    return mTitle;
}

QString Entry::author() const
{
    return mAuthor;
}

QString Entry::content() const
{
    //return mContent.toHtml();
    return mContent.toPlainText();
}

QString Entry::contentDirection() const
{
    return mContentDirection;
}

QString Entry::alternativeLink() const
{
    return mAlternativeLink;
}

QString Entry::alternativeType() const
{
    return mAlternativeType;
}

QString Entry::canonicalLink() const
{
    return mCanonicalLink;
}

QString Entry::fingerprint() const
{
    return mFingerprint;
}

QString Entry::sid() const
{
    return mSid;
}

double Entry::published() const
{
    return mPublished;
}

double Entry::updated() const
{
    return mUpdated;
}

double Entry::crawled() const
{
    return mCrawled;
}

double Entry::recrawled() const
{
    return mRecrawled;
}

int Entry::engagement() const
{
    return mEngagement;
}

bool Entry::unread() const
{
    return mUnread;
}


// **************** model ******************

EntriesModel::EntriesModel() :
    QAbstractListModel()
{
    mRoleNames = roleNames();
    mCurrentIndex = -1;
}

EntriesModel::~EntriesModel()
{
    clear();
}


void EntriesModel::markEntryAsRead(const int &index)
{
    qDebug() << "EntriesModel::markEntryAsRead called";
    if(index > (mEntries.count() -1))
        return;
    if(mEntries.at(index)->unread() == true) {
        mEntries.at(index)->setUnread(false);

        QModelIndex modelIndex = indexFromFeed(mEntries.at(index));
        emit dataChanged(modelIndex, modelIndex);
    }
}

int EntriesModel::markAllEntriesRead()
{
    qDebug() << "EntriesModel::markAllEntriesRead called";
    int count = 0;
    foreach(Entry *ent, mEntries) {
        if(ent->unread() == true) {
            ent->setUnread(false);
            count++;
        }
    }
    emit dataChanged(index(0), index(mEntries.size() - 1));
    return count;
}

QString EntriesModel::getEntryUrlAtIndex(const int &index)
{
    return mEntries.at(index)->alternativeLink();
}

int EntriesModel::currentIndex() const
{
    return mCurrentIndex;
}

bool EntriesModel::isEmpty() const
{
    return mEntries.isEmpty();
}

void EntriesModel::clear()
{
    qDeleteAll(mEntries.begin(), mEntries.end());
    mEntries.clear();
}

Entry* EntriesModel::at(const int &index)
{
    Entry *entry = mEntries.at(index);
    return entry;
}

void EntriesModel::insert(int row, Entry *entry)
{
    beginInsertRows(QModelIndex(), row, row);
    //connect(feed, SIGNAL(dataChanged()), SLOT(handleItemChange()));
    mEntries.insert(row, entry);
    QModelIndex modelIndex = indexFromFeed(mEntries.last());
    emit dataChanged(modelIndex, modelIndex);
    endInsertRows();
}

void EntriesModel::append(Entry *entry)
{
    mEntries.append(entry);
    QModelIndex modelIndex = indexFromFeed(mEntries.last());
    emit dataChanged(modelIndex, modelIndex);
}

int EntriesModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mEntries.count();
}

QVariant EntriesModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row() < 0 || index.row() > (mEntries.count() - 1))
        return QVariant();

    //qDebug() << "EntriesModel::data entered";
    //qDebug() << "EntriesModel::date " << "role: " <<  role << " index " << index.row();

    //mCurrentIndex = index.row();

    switch(role) {
    case Content:
        return mEntries.at(index.row())->content();
        break;
    case Title:
        return mEntries.at(index.row())->title();
        break;
    case EntryId:
        mCurrentIndex = index.row();
        return mEntries.at(index.row())->entryId();
        break;
    case Unread:
        return mEntries.at(index.row())->unread();
        break;
    default:
        //mCurrentIndex = -1;
        break;
    };
    return QVariant();
}

// protected
QHash<int, QByteArray> EntriesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Content] = "content";
    roles[Title] = "title";
    roles[EntryId] = "entryId";
    roles[Unread] = "unread";

    return roles;
}

// private
QModelIndex EntriesModel::indexFromFeed(const Entry *entry)
{
    for(int row = 0; row < mEntries.size(); ++row) {
        if(mEntries.at(row) == entry)
            return index(row);
    }
    return QModelIndex();
}



















