#ifndef ENTRIESMODEL_H
#define ENTRIESMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QTextDocument>
#include <QString>
#include <QList>

class EntriesModel;

class Entry : public QObject
{
    Q_OBJECT
public:
    Entry();
    ~Entry();

    // setters
    void setEntryId(const QString &id);
    void setCategoryId(const QString &id);
    void setCategoryLabel(const QString &label);
    void setOriginHtmlUrl(const QString &url);
    void setOriginId(const QString &id);
    void setOriginTitle(const QString &title);
    void setStreamId(const QString &id);
    void setTitle(const QString &title);
    void setAuthor(const QString &author);
    void setContent(const QString &content);
    void setContentDirection(const QString &direction);
    void setAlternativeLink(const QString &link);
    void setAlternativeType(const QString &type);
    void setCanonicalLink(const QString &link);
    void setFingerprint(const QString &value);
    void setSid(const QString &id);
    void setPulished(const double &timestamp);
    void setUpdated(const double &timestamp);
    void setCrawled(const double &timestamp);
    void setRecrawled(const double &timestamp);
    void setEngagement(const int &value);
    void setUnread(const bool &trueFalse);
    // getters
    QString entryId() const;
    QString categoryId() const;
    QString categoryLabel() const;
    QString originHtmlUrl() const;
    QString originId() const;
    QString originTitle() const;
    QString streamId() const;
    QString title() const;
    QString author() const;
    QString content() const;
    QString contentDirection() const;
    QString alternativeLink() const;
    QString alternativeType() const;
    QString canonicalLink() const;
    QString fingerprint() const;
    QString sid() const;
    double published() const;
    double updated() const;
    double crawled() const;
    double recrawled() const;
    int engagement() const;
    bool unread() const;
private:
    Q_DISABLE_COPY(Entry)
    QString mEntryId;
    QString mCategoryId;
    QString mCategoryLabel;
    QString mOriginHtmlUrl;
    QString mOriginId;
    QString mOriginTitle;
    QString mStreamId;
    QString mTitle;
    QString mAuthor;
    //QString mContent;
    QTextDocument mContent;

    QString mContentDirection;
    QString mAlternativeLink;
    QString mAlternativeType;
    QString mCanonicalLink;
    QString mFingerprint;
    QString mSid;
    double mPublished;
    double mUpdated;
    double mCrawled;
    double mRecrawled;
    int mEngagement;
    bool mUnread;

};

class EntriesModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        Content = Qt::UserRole + 1,
        Title,
        EntryId,
        Unread
    };
    EntriesModel();
    ~EntriesModel();

    Q_INVOKABLE void markEntryAsRead(const int &index);
    Q_INVOKABLE int markAllEntriesRead();
    Q_INVOKABLE QString getEntryUrlAtIndex(const int &index);

    int currentIndex() const;
    bool isEmpty() const;
    void clear();

    Entry* at(const int &index);
    void insert(int row, Entry *entry);
    void append(Entry *entry);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    Q_DISABLE_COPY(EntriesModel)
    QModelIndex indexFromFeed(const Entry *entry);

    QList<Entry* > mEntries;
    QHash<int, QByteArray> mRoleNames;
    mutable int mCurrentIndex;
};


#endif // ENTRIESMODEL_H
