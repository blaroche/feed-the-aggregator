#ifndef FEEDSMODEL_H
#define FEEDSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QString>
#include <QList>


class FeedModel;

class Feed : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(bool isOpened READ isOpened WRITE setIsOpened NOTIFY isOpenedChanged)
    Q_PROPERTY(bool hasUnread READ hasUnread NOTIFY hasUnreadChanged)
    Q_PROPERTY(bool unreadCount READ unreadCount WRITE setUnreadCount NOTIFY isUnreadChanged)
public:
    Feed();
    ~Feed();
    bool isEmpty() const;
    inline bool hasChildren() const;
    bool hasUnread();
    void adjustLevels();
    void addUnreadCount();
    void subtractUnreadCount(const int &count = 1);

    // setters
    void setId(const QString &id);
    void setLabel(const QString &label);
    void setSortId(const QString &id);
    void setWebsite(const QString &website);
    void setUnreadCount(const double &count);
    void setUpdated(const double &updated);
    void setVelocity(const double &value);
    void setIsOpened(const bool &opened);
    void setIsSubscription(const bool &trueFalse);
    void setIsCategory(const bool &trueFalse);
    void setLevel(const int &level);
    //void setFeedModel(FeedModel *model);
    void appendFeedModel(Feed *feed);
    // getters
    QString id() const;
    QString label() const;
    QString sortId() const;
    QString webSite() const;
    double unreadCount() const;
    double updated() const;
    double velocity() const;
    bool isOpened() const;
    bool isSubscription() const;
    bool isCategory() const;
    int level() const;
    FeedModel* feedModel() const;
    //FeedModel& feedModel() const;

signals:
    void isOpenedChanged();
    void isUnreadChanged();
    void hasUnreadChanged();
private:
    Q_DISABLE_COPY(Feed)

    QString mId;
    QString mLabel;
    QString mSortId;
    QString mWebsite;
    double mUnreadCount;
    double mUpdated;
    double mVelocity;
    bool mIsOpened;
    bool mIsSubscription;
    bool mIsCategory;
    int mLevel;
    FeedModel *mFeedModel;
    //FeedModel mFeedModel;
};


class FeedModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(double globalUnreadItems READ globalUnreadItems WRITE setGlobalUnreadItem NOTIFY globaleUnreadItemsChanged)
public:
    enum Roles {
        Id = Qt::UserRole + 1,
        Label,
        UnreadCount,
        Level,
        OpenRow,
        CloseRow,
        IsOpened,
        IsSubscription,
        IsCategory,
        HasUnread,
        HasChildren
    };
    FeedModel();
    ~FeedModel();

    Q_INVOKABLE void openRow(const int &index);
    Q_INVOKABLE void closeRow(const int &index);

    Q_INVOKABLE void markFeedAsRead(const int &index);
    Q_INVOKABLE void markFeedsAsRead(const int &index, const int &count);

    // depreciated methods
    void subtractGlobalUnreadItems(const int &count);
    Feed* findFeedById(const QString &id);

    int currentIndex() const;
    void doLeveling();
    void setGlobalUnreadItem(const double &count);
    double globalUnreadItems() const;

    bool isEmpty() const;
    void clear();

    QList<Feed* > feeds() const;
    Feed* findCategoryById(const QString &id);

    Feed* findFirstFeedById(const QString &id);
    QList<Feed*> findFeedsById(const QString &id);
    void updateUnreadById(const QString &id, const double &count,
                            const double &updated);
    Feed* at(const int &index);
    void insert(int row, Feed *feed);
    void removeAt(int row);
    void append(Feed *feed);
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
signals:
    void globaleUnreadItemsChanged();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    Q_DISABLE_COPY(FeedModel)
    QModelIndex indexFromFeed(const Feed *feed);
    QList<Feed* > mFeeds;
    QHash<int, QByteArray> mRoleNames;
    double mGlobalUnreadItems;
    mutable int mCurrentIndex;
};






#endif // FEEDSMODEL_H











