#include "profilemodel.h"

ProfileModel::ProfileModel()
{
    mEvernoteConnected = false;
    mPocketConnected = false;
    mWordPressConnected = false;

}

// setters
void ProfileModel::setId(const QString &id)
{
    mId = id;
    emit idChanged();
}

void ProfileModel::setEmail(const QString &email)
{
    mEmail = email;
    emit emailChanged();
}

void ProfileModel::setFullName(const QString &name)
{
    mFullName = name;
    emit fullNameChanged();
}

void ProfileModel::setGivenName(const QString &givenName)
{
    mGivenName = givenName;
    emit givenNameChanged();
}

void ProfileModel::setFamilyName(const QString &familyName)
{
    mFamilyName = familyName;
    emit familyNameChanged();
}

void ProfileModel::setPicture(const QString &picture)
{
    mPicture = picture;
    emit pictureChanged();
}

void ProfileModel::setGender(const QString &gender)
{
    mGender = gender;
    emit genderChanged();
}

void ProfileModel::setLocale(const QString &locale)
{
    mLocale = locale;
    emit localeChanged();
}

void ProfileModel::setGoogle(const QString &google)
{
    mGoogle = google;
    emit googleChanged();
}

void ProfileModel::setTwitter(const QString &twitter)
{
    mTwitter = twitter;
    emit twitterChanged();
}

void ProfileModel::setFacebook(const QString &facebook)
{
    mFacebook = facebook;
    emit facebookChanged();
}

void ProfileModel::setWave(const QString &wave)
{
    mWave = wave;
    emit waveChanged();
}

void ProfileModel::setClient(const QString &client)
{
    mClient = client;
    emit clientChanged();
}

void ProfileModel::setEvernoteConnected(const bool &isConnected)
{
    mEvernoteConnected = isConnected;
    emit evernoteConnectedChanged();
}

void ProfileModel::setPocketConnected(const bool &isConnected)
{
    mPocketConnected = isConnected;
    emit pocketConnectedChanged();
}

void ProfileModel::setWordPressConnected(const bool &isConnected)
{
    mWordPressConnected = isConnected;
    emit wordPressConnectedChanged();
}

// getters
const QString& ProfileModel::id() const
{
    return mId;
}

const QString& ProfileModel::email() const
{
    return mEmail;
}

const QString& ProfileModel::fullName() const
{
    return mFullName;
}

const QString& ProfileModel::givenName() const
{
    return mGivenName;
}

const QString& ProfileModel::familyName() const
{
    return mFamilyName;
}

const QString& ProfileModel::picture() const
{
    return mPicture;
}

const QString& ProfileModel::gender() const
{
    return mGender;
}

const QString& ProfileModel::locale() const
{
    return mLocale;
}

const QString& ProfileModel::google() const
{
    return mGoogle;
}

const QString& ProfileModel::twitter() const
{
    return mTwitter;
}

const QString& ProfileModel::facebook() const
{
    return mFacebook;
}

const QString& ProfileModel::wave() const
{
    return mWave;
}

const QString& ProfileModel::client() const
{
    return mClient;
}

const bool& ProfileModel::evernoteConnected() const
{
    return mEvernoteConnected;
}

const bool& ProfileModel::pocketConnected() const
{
    return mPocketConnected;
}

const bool& ProfileModel::wordPressConnected() const
{
    return mWordPressConnected;
}














