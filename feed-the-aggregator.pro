# CONFIG += qt warn_off debug
# DEFINES += QMLJSDEBUGGER
QT += gui sql core xml quick network webkit

# TARGET = Feedly
# DESTDIR = ../

SOURCES += \
    main.cpp \
    viewControllers/viewcontroller.cpp \
    viewControllers/mainwindow.cpp \
    libs/networkaccessmanager.cpp \
    libs/logger.cpp \
    libs/jsonparser.cpp \
    libs/feedlyapi.cpp \
    libs/version.cpp \
    models/preferencesmodel.cpp \
    libs/feedlyparser.cpp \
    models/profilemodel.cpp \
    models/feedsmodel.cpp \
    models/entriesmodel.cpp

HEADERS += \
    viewControllers/viewcontroller.h \
    viewControllers/mainwindow.h \
    libs/networkaccessmanager.h \
    libs/logger.h \
    libs/jsonparser.h \
    apikey.h \
    config.h \
    libs/feedlyapi.h \
    libs/version.h \
    models/preferencesmodel.h \
    libs/feedlyparser.h \
    models/profilemodel.h \
    models/feedsmodel.h \
    models/entriesmodel.h

OTHER_FILES += \
    qml/MainWindow.qml \
    qml/MainPage.qml \
    qml/components/MainPageTabTools.qml \
    qml/AuthPage.qml \
    qml/StartupPage.qml \
    qml/components/NavigationHeader.qml \
    qml/components/ExampleModel.qml \
    qml/components/NavigationListView.qml \
    qml/components/EntriesListView.qml \
    qml/EntryViewPage.qml \
    qml/components/HeaderTest.qml \
    qml/components/EntryHeader.qml \
    qml/EntryWebView.qml \
    qml/components/EntryWebViewTools.qml \
    qml/components/EntryViewTools.qml

RESOURCES += \
    qmlComponents.qrc \
    qmlMainFiles.qrc \
    qml/components/img.qrc \
    misc.qrc


# run test suit,
# clone a build in projects and add additional args of CONFIG += test to run below block
test {
    message(Test build)
    QT += testlib
    QT += widgets
    TARGET = tests

    SOURCES -= main.cpp

    SOURCES += tests/main.cpp \
        tests/feedlyapitest.cpp \
        tests/parseprofiletest.cpp \
        tests/parsecategoriestest.cpp \
        tests/parsesubscriptionstest.cpp \
        tests/parsemarkerstest.cpp \
        tests/parseentriestest.cpp \
        tests/feedandentrymodeltest.cpp \
        tests/feedmodeltest.cpp

    HEADERS += \
        tests/feedlyapitest.h \
        tests/parseprofiletest.h \
        tests/parsecategoriestest.h \
        tests/parsesubscriptionstest.h \
        tests/parsemarkerstest.h \
        tests/parseentriestest.h \
        tests/feedmodeltest.h \
        tests/feedandentrymodeltest.h \
        tests/data/feedlyapidata.h

} else {
    message(Normal build)
}
















