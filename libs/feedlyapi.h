#ifndef FEEDLYAPI_H
#define FEEDLYAPI_H

#include <QObject>

#include "networkaccessmanager.h"
#include "feedlyparser.h"
//#include "jsonparser.h"

class Logger;
class CategoryModel;


class FeedlyApi : public NetworkAccessManager
{
    Q_OBJECT
public:
    explicit FeedlyApi();

    bool authRequest();
    bool profileRequest();
    bool preferencesRequest();
    bool categoriesRequest();
    bool subscriptionsRequest();
    bool markersRequest();
    bool entriesMgetFromFeedId(const QString &feedId);


    // depreciated methods
    bool beginAuth();
    bool getProfile();
    bool getPreferences();
    bool getCategories();
    bool getSubscriptions();
    bool getMarkers();
    bool getStream(const QString &id);
    bool getEntriesMgetFromFeedId(const QString &feedId);
    bool markEntryAsRead(const QString &id);
    bool markFeedAsRead(const QString &id);
    // end depreciated methods

    void parseResults(const bool &onOff);
    QString printResults();
    QNetworkReply::NetworkError getFeedlyError() const;
    FeedModel* getFeedsModel() const;
    EntriesModel* getEntriesModel() const;

    Q_INVOKABLE void parseAuthorizationResponse(const QString &response);
    // setters
    void setAccessToken(const QString &token);
signals:
    void loadAuthorizationPage(const QString &html, const QString &url);
    void authCompleted();
public slots:
    void gotAccessToken(const QString &accessToken);

private:
    void setRequestHeader();
    void getAuthorizationToken(const QString &code, const QString &state = "");
    void handleError();

    Logger *logger;
    FeedlyParser parser;
    QString mAccessToken;
    bool willParseResults;

};

#endif // FEEDLYAPI_H


/*
feed/http://aseigo.blogspot.com/feeds/posts/default
*/
