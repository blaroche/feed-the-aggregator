#ifndef VERSION_H
#define VERSION_H

#include <QString>

class Version
{
public:
    Version();
    Version(const Version &version);
    Version(const QString &version);
    QString toString() const;
    QString toSimpleString() const;
    void clear();

    Version &operator=(const Version &version);
    Version &operator=(const QString &version);
    bool operator==(const Version &version) const;
    bool operator!=(const Version &version) const;
    bool operator>=(const Version &version) const;
    bool operator>(const Version &version) const;
    bool operator!() const;

    // setters
    void setMajor(int major);
    void setMinor(int minor);
    void setPatch(int patch);
    // getters
    int getMajor() const;
    int getMinor() const;
    int getPatch() const;
private:
    int major;
    int minor;
    int patch;
};

#endif // VERSION_H

