#include <QDebug>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QVariantList>
#include <QStringList>


#include <QMapIterator>

#include "feedlyparser.h"
#include "models/preferencesmodel.h"
#include "models/profilemodel.h"
#include "models/entriesmodel.h"
#include "logger.h"

FeedlyParser::FeedlyParser()
{
    profileModel = NULL;
    preferencesModel = NULL;
    //subscriptionModel = NULL;
    subscription = NULL;
    categoryModel = NULL;
    category = NULL;
    subscriptionCategoryIds = NULL;

    entriesModel = NULL;
    entry = NULL;
    apiMethod = None;
    entryObject = EntryNone;
    objectCounter = 0;
}

FeedlyParser::~FeedlyParser()
{
    if(profileModel)
        delete profileModel;
    if(preferencesModel)
        delete preferencesModel;
    //if(subscription)
    //    delete subscription;
    if(categoryModel)
        delete categoryModel;
    //if(category)
    //    delete category;
    if(subscriptionCategoryIds)
        delete subscriptionCategoryIds;
    //if(entry))   // pointed to gets deleted wtih entriesModel
    //    delete entry;
    if(entriesModel)
        delete entriesModel;
}

bool FeedlyParser::parseResponse(QString &jsonResponse, Method method)
{
    #ifdef DEBUG
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    bool result;
    this->apiMethod = method;

    initModels();
    result = parseJsonResponse(jsonResponse);
    cleanupModels();

    return result;
}

ProfileModel* FeedlyParser::getProfileModel() const
{
    if(profileModel)
        return profileModel;
    return NULL;
}

FeedModel* FeedlyParser::getFeedsModel() const
{
    return categoryModel;
}

FeedModel* FeedlyParser::getCategoryModel() const
{
    return categoryModel;
}

EntriesModel* FeedlyParser::getEntriesModel() const
{
    return entriesModel;
}

// private
void FeedlyParser::initModels()
{
    objectCounter = 0;
    switch(apiMethod) {
    case AuthToken:
        break;
    case Profile:
        if(profileModel == NULL)
            profileModel = new ProfileModel();
        break;
    case Preferences:
        if(preferencesModel == NULL)
            preferencesModel = new PreferencesModel();
        break;
    case Categories:
        if(categoryModel == NULL)
            categoryModel = new FeedModel();
        else {
            categoryModel->clear();
            //delete categoryModel;
            //categoryModel = new FeedModel();
        }
        break;
    case Subscriptions:
        //if(categoryModel == NULL)
        //    categoryModel = new FeedModel();
        //if(subscriptionModel == NULL)
        //    subscriptionModel = new SubscriptionModel();
        break;
    case EntriesMget:
        if(entriesModel == NULL)
            entriesModel = new EntriesModel();
        else {
            entriesModel->clear();
            //delete entriesModel;
            //entriesModel = new EntriesModel();
        }
        break;
    default:
        break;
    }
}

void FeedlyParser::cleanupModels()
{
    switch(apiMethod) {
    case AuthToken:
        break;
    case Profile:
        break;
    case Preferences:
        break;
    case Categories:
        //categoryModel->append(category);
        //if(category != NULL) {
        //    delete category;
        //    category = NULL;
        //}
        break;
    case Subscriptions:
        //if(subscription != NULL) {
        //    if(!subscription->isEmpty())
        //        subscriptionModel->appendRow(subscription);
            //delete subscription;
        //    subscription = NULL;
        //}

        break;
    default:
        break;
    }
}

QVariant FeedlyParser::parseJsonObject(const QJsonObject &object)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    QVariant result = QVariant::Invalid;

    switch(apiMethod) {
    case Categories:
        if(category == NULL)
            category = new Feed();
        break;
    case Subscriptions:
        if(subscription == NULL)
            subscription = new Feed();
        break;
    case EntriesMget:
        if(entry == NULL) {
            entry = new Entry();
            entryObject = EntryNone;
            parseEntries(object);
            entriesModel->append(entry);
            entry = NULL;
            return QVariant("Entries");
        }
        break;
    default:
        break;
    }

    result = JsonParser::parseJsonObject(object);

    switch(apiMethod) {
    case Categories:
        if(category) {
            categoryModel->append(category);
            category = NULL;
        }
        break;
    case Subscriptions: {
        if(subscription) {
            if(!subscription->isEmpty()) {
                if(subscriptionCategoryIds->size() > 0) {
                    for(int i = 0; i < subscriptionCategoryIds->size(); ++i) {
                        if(categoryModel)
                            categoryModel->findFeedById(subscriptionCategoryIds->at(i))->appendFeedModel(subscription);
                    }
                }
                else {  // add uncategorized
                    if(category == NULL)
                        category = categoryModel->findFeedById("global.uncategorized");

                    if(category == NULL) {
                        category = new Feed();
                        categoryModel->append(category);
                    }
                    category->setLabel("Uncategorized");
                    category->setId("global.uncategorized");
                    category->setIsCategory(true);

                    category->appendFeedModel(subscription);
                }
                category = NULL;
                subscription = NULL;
            }
            if(subscriptionCategoryIds != NULL) {
                delete subscriptionCategoryIds;
                subscriptionCategoryIds = NULL;
            }
        }
        break;
    }
    default:
        break;
    }

    return result;
}

QVariant FeedlyParser::parseJsonValue(const QJsonValue &value, const QString &keyName)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    QVariant result = QVariant::Invalid;

    switch(apiMethod) {
    case Subscriptions:
        if(value.isArray()) {
            if(keyName == "categories") {
                parseSubscriptionCategories(value.toArray().toVariantList());
                return QVariant("Categories");
            }
        }
        break;
    case Markers:
        if(value.isArray()) {
            if(keyName == "unreadcounts") {
                //qDebug() << "\n\n array: " << value.toArray() << "\n";
                parseMarkers(value.toArray().toVariantList());
                return QVariant("Markers");
            }
        }
        break;
    default:
        break;
    }

    result = JsonParser::parseJsonValue(value, keyName);


    return result;
}

void FeedlyParser::parseString(const QString &value, const QString &keyName)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, QString("value: %1 - key: %2").arg(value).arg(keyName));
    #endif
    switch(apiMethod) {
    case AuthToken:
        if(settings.isWritable())
            settings.setValue(keyName, value);
        else
            qDebug() << "CANT WRITE TO SETTINGS!!!!!!!!!";
        //emit gotAccesstoken();
        break;
    case Profile: {
        if(keyName == "client")
            profileModel->setClient(value);
        else if(keyName == "email")
            profileModel->setEmail(value);
        else if(keyName == "familyName")
            profileModel->setFamilyName(value);
        else if(keyName == "givenName")
            profileModel->setGivenName(value);
        else if(keyName == "google")
            profileModel->setGoogle(value);
        else if(keyName == "id")
            profileModel->setId(value);
        else if(keyName == "locale")
            profileModel->setLocale(value);
        else if(keyName == "wave")
            profileModel->setWave(value);
        else if(keyName == "picture")
            profileModel->setPicture(value);
        else if(keyName == "twitter")
            profileModel->setTwitter(value);
        else if(keyName == "facebook")
            profileModel->setFacebook(value);
        else if(keyName == "fullName")
            profileModel->setFullName(value);
        else if(keyName == "gender")
            profileModel->setGender(value);
        else
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled Profile key: %1").arg(keyName));
        break;
    }
    case Categories: {
        if(keyName == "id")
            category->setId(value);
        else if(keyName == "label")
            category->setLabel(value);
        else
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled Category key: %1").arg(keyName));
        category->setIsCategory(true);
        break;
    }
    case Subscriptions: {
        if(subscription) {
            if(keyName == "id") {
                subscription->setId(value);
            }
            else if(keyName == "title")
                subscription->setLabel(value);
            else if(keyName == "sortid")
                subscription->setSortId(value);
            else if(keyName == "website")
                subscription->setWebsite(value);
            subscription->setIsSubscription(true);
            //else if(keyName == "velocity")
                //TODO:: to this;
                //updated
        }
        else
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled Subscription key: %1").arg(keyName));

        break;
    }
    case EntriesMget:
        if(entry) {
            switch(entryObject) {
            case EntryCategories:
                if(keyName == "id")
                    entry->setCategoryId(value);
                else if(keyName == "label")
                    entry->setCategoryLabel(value);
                else
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry cat key: %1").arg(keyName));
                break;
            case EntryContent:
                if(keyName == "content")
                    entry->setContent(value);
                else if(keyName == "direction")
                    entry->setContentDirection(value);
                else
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry content key: %1").arg(keyName));
                break;
            case EntryAlternative:
                if(keyName == "href")
                    entry->setAlternativeLink(value);
                else if(keyName == "type")
                    entry->setAlternativeType(value);
                else
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry alt key: %1").arg(keyName));
                break;
            case EntryOrigin:
                if(keyName == "streamId")
                    entry->setStreamId(value);
                else if(keyName == "htmlUrl")
                    entry->setOriginHtmlUrl(value);
                else if(keyName == "title")
                    entry->setOriginTitle(value);
                else
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry origin key: %1").arg(keyName));
                break;
            case EntryNone:
                if(keyName == "title")
                    entry->setTitle(value);
                else if(keyName == "author")
                    entry->setAuthor(value);
                else if(keyName == "id")
                    entry->setEntryId(value);
                else if(keyName == "fingerprint")
                    entry->setFingerprint(value);
                else if(keyName == "originId")
                    entry->setOriginId(value);
                else if(keyName == "sid")
                    entry->setSid(value);
                else {
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry key: %1").arg(keyName));
                    logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry value: %1").arg(value));

                }
                break;
            default:
                logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry key: %1").arg(keyName));
                logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry value: %1").arg(value));
                break;
            }
        }
        break;
    default:
        break;
    }
}

void FeedlyParser::parseDouble(const double &value, const QString &keyName)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, QString("value: %1 - key: %2").arg(value).arg(keyName));
    #endif

    Q_UNUSED(value);
    Q_UNUSED(keyName);

    switch(apiMethod) {
    case AuthToken:
        break;
    case Profile:
        break;
    case Subscriptions:
        if(subscription) {
            if(keyName == "updated")
                subscription->setUpdated(value);
            else if(keyName == "velocity")
                subscription->setVelocity(value);
        }
        break;
    case EntriesMget:
        if(keyName == "published")
            entry->setPulished(value);
        else if(keyName == "updated")
            entry->setUpdated(value);
        else if(keyName == "crawled")
            entry->setCrawled(value);
        else if(keyName == "recrawled")
            entry->setRecrawled(value);
        else {
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry key: %1").arg(keyName));
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled entry value: %1").arg(value));
        }
        break;
    default:
        break;
    }
}

void FeedlyParser::parseBool(const bool &value, const QString &keyName)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, QString("value: %1 - key: %2").arg(value).arg(keyName));
    #endif
    Q_UNUSED(value);
    Q_UNUSED(keyName);

    switch(apiMethod) {
    case AuthToken:
        break;
    case Profile:
        if(keyName == "evernoteConnected")
            profileModel->setEvernoteConnected(value);
        else if(keyName == "pocketConnected")
            profileModel->setPocketConnected(value);
        else if(keyName == "wordPressConnected")
            profileModel->setWordPressConnected(value);
        else
            logger->error(DEBUG_FUNCTION_NAME, QString("unhandled Profile key: %1").arg(keyName));
        break;
    case EntriesMget:
        if(keyName == "unread")
            entry->setUnread(value);

        break;
    default:
        break;
    }
}


void  FeedlyParser::parseSubscriptionCategories(const QVariantList &subscriptionList)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    QVariantList list = subscriptionList;
    //if(list.isEmpty())
    //    list << "Uncategorized";
    if(subscriptionCategoryIds == NULL)
        subscriptionCategoryIds = new QStringList();
    else
        subscriptionCategoryIds->clear();

    QVariantList::iterator i;
    for(i = list.begin(); i != list.end(); ++i) {
        QVariantMap map = i->toMap();
        QMap<QString, QVariant>::const_iterator i = map.constBegin();

        while (i != map.constEnd()) {
            if(i.key() == "id")
                subscriptionCategoryIds->append(i.value().toString());
            ++i;
        }
    }
}

void FeedlyParser::parseCategories(const QVariantList &categoryList)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    QVariantList list = categoryList;
    QVariantList::iterator i;
    for(i = list.begin(); i != list.end(); ++i) {
        Feed category;

        QVariantMap map = i->toMap();
        QMap<QString, QVariant>::const_iterator i = map.constBegin();

        while (i != map.constEnd()) {
            if(i.key() == "id")
                category.setId(i.value().toString());
            else if(i.key() == "label")
                category.setLabel(i.value().toString());
            ++i;
        }
        if(subscription != NULL) {
            //subscription->categoryModel().append(category);
        }
    }
}

void FeedlyParser::parseMarkers(const QVariantList &markerList)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif
    if(categoryModel == NULL)
        return;

    QVariantList list = markerList;
    QVariantList::iterator i;
    for(i = list.begin(); i != list.end(); ++i) {
        QString id;
        double updated;
        double count;

        QVariantMap map = i->toMap();
        QMap<QString, QVariant>::const_iterator i = map.constBegin();

        while (i != map.constEnd()) {
            if(i.key() == "id")
                id = i.value().toString();
            else if(i.key() == "count")
                count = i.value().toDouble();
            else if(i.key() == "updated")
                updated = i.value().toDouble();
            ++i;
        }
        QStringList list = id.split("/");
        if(list[0] == "feed") {
        //    if(subscriptionModel)
        //        subscriptionModel->updateUnreadCountsById(id, count, updated);
            for(int i = 0; i < categoryModel->rowCount(); i++) {
                //categoryModel[i]->subsc-
                categoryModel->at(i)->feedModel()->updateUnreadById(id, count, updated);
            }
            //foreach(Category cat, categoryModel) {
            //    cat.subscriptionModel()->updateUnreadCountsById(id, count, updated);
            //}
        }
        else if(list[0] == "user") {
            if(categoryModel)
                categoryModel->updateUnreadById(id, count, updated);
            if(list.last() == "global.uncategorized") {
                if(categoryModel)
                    categoryModel->updateUnreadById("global.uncategorized", count, updated);
            }
            else if(list.last() == "global.all") {
                if(categoryModel)
                    categoryModel->setGlobalUnreadItem(count);
            }
        }
    }
}

void FeedlyParser::parseEntries(const QJsonObject &object)
{
    #ifdef DEBUG
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    #endif

    //qDebug() << "\n\nEntry\n";
    //qDebug() << object;

    foreach(QString key, object.keys()) {
        QJsonValue value = object.value(key);
        //qDebug() << "key: " << key;
        //qDebug() << "value: " << value;
        if(value.isArray()) {
            if(key == "alternate")
               entryObject = EntryAlternative;
            else if(key == "categories")
                entryObject = EntryCategories;
            else if(key == "tags") {
                //TODO:: add support for tags, skipping for now
                return;
            }
            else if(key == "thumbnail") {
                return;
            }
            else if(key == "keywords") {
                //TODO:: add support for keywords, skipping for now
                //return;
            }
            //qDebug() << "isArray\n";
            JsonParser::parseJsonArray(value.toArray());
        }
        else if(value.isObject()) {
            if(key == "content")
                entryObject = EntryContent;
            else if(key == "origin")
                entryObject = EntryOrigin;
            else if(key == "summary") {
                //qDebug() << "\nsummary keys: " << value.toObject().keys();
                entryObject = EntryContent;
            }
            else if(key == "visual") {
                return;
            }

            //qDebug() << "isObject\n";
            JsonParser::parseJsonObject(value.toObject());
        }
        else {
            entryObject = EntryNone;
            //qDebug() << "is none\n";
            JsonParser::parseJsonValue(value, key);
        }

    }
}






























