#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QStringList>

#include "jsonparser.h"
#include "logger.h"

JsonParser::JsonParser()
{
    logger = Logger::instance();
}

bool JsonParser::parseJsonResponse(const QString &jsonResponse)
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QVariant result;
    QJsonDocument doc = QJsonDocument::fromJson(jsonResponse.toUtf8());

    if(doc.isNull()) {
        logger->error(DEBUG_FUNCTION_NAME, "Invalid json response");
        return false;
    }
    else if(doc.isEmpty()) {
        logger->warning(DEBUG_FUNCTION_NAME, "Empty json response");
        return false;
    }
    else if(doc.isObject()) {
        result = parseJsonObject(doc.object());
    }
    else if(doc.isArray()) {
        result = parseJsonArray(doc.array());
    }

    if(result == QVariant::Invalid)
        return false;
    return true;
}

// private
QVariant JsonParser::parseJsonObject(const QJsonObject &object)
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QVariant result = QVariant::Invalid;

    if(object.isEmpty()) {
        logger->warning(DEBUG_FUNCTION_NAME, "Empty response, Json object empty");
        return result;
    }
    foreach (QString key, object.keys()) {
        QJsonValue value = object.value(key);
        result = parseJsonValue(value, key);
    }
    return result;
}

QVariant JsonParser::parseJsonArray(const QJsonArray &array)
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QVariant result = QVariant::Invalid;

    if(array.isEmpty()) {
        logger->warning(DEBUG_FUNCTION_NAME, "Empty response, json array empty");
        return result;
    }

    for(int i = 0; i < array.size(); i++) {
        if(array.at(i).isObject()) {
            result = parseJsonObject(array.at(i).toObject());
        }
    }
    return result;
}

QVariant JsonParser::parseJsonValue(const QJsonValue &value, const QString &keyName)
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QVariant result = QVariant::Invalid;

    if(value.isBool()) {
        //logger->debug(DEBUG_FUNCTION_NAME, QString("value is bool: %1 - key: %2").arg(value.toString()).arg(keyName));
        parseBool(value.toBool(), keyName);
        result = value.toBool();
    }
    else if(value.isDouble()) {
        //logger->debug(DEBUG_FUNCTION_NAME, QString("value is double: %1 - key: %2").arg(value.toString()).arg(keyName));
        parseDouble(value.toDouble(), keyName);
        result = value.toDouble();
    }
    else if(value.isNull()) {
        logger->debug(DEBUG_FUNCTION_NAME, QString("value is null: %1 - key: %2").arg(value.toString()).arg(keyName));
    }
    else if(value.isString()) {
        //logger->debug(DEBUG_FUNCTION_NAME, QString("value is string: %1 - key: %2").arg(value.toString()).arg(keyName));
        parseString(value.toString(), keyName);
        //if(!value.toString().isEmpty())
        //    result = value.toString();
    }
    else if(value.isUndefined()) {
        logger->debug(DEBUG_FUNCTION_NAME, QString("value undefined: %1 - key: %2").arg(value.toString()).arg(keyName));
    }
    else if(value.isArray()) {
        //logger->debug(DEBUG_FUNCTION_NAME, QString("value is array, key: %2").arg(keyName));
        result = parseJsonArray(value.toArray());
    }
    else if(value.isObject()) {
        //logger->debug(DEBUG_FUNCTION_NAME, QString("value is object: %1 - key: %2").arg(value.toString()).arg(keyName));
        parseJsonObject(value.toObject());
    }
    else {
        logger->error(DEBUG_FUNCTION_NAME, QString("unhandled %1, %2").arg(value.String).arg(keyName));
    }
    return result;
}
































