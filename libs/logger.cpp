#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QMutex>

#include "logger.h"

Logger* Logger::s_instance = 0;

Logger* Logger::instance()
{
    if (s_instance == 0) {
        QMutex mutex;
        mutex.lock();
        s_instance = new Logger();
        mutex.unlock();
    }
    return s_instance;
}

void Logger::destroy()
{
    QMutex mutex;
    mutex.lock();
    delete s_instance;
    s_instance = 0;
    mutex.unlock();
}

void Logger::enableConsoleDebug(bool enabled)
{
    consoleDebugEnabled = enabled;
}

void Logger::enableLogFileDebug(bool enabled)
{
    logFileEnabled = enabled;
}

void Logger::enableLogFileAppend(bool enabled)
{
    appendEnabled = enabled;
}

void Logger::debug(const QString &className, const QString &message)
{
    instance()->log(className, "DEBUG", message);
}

void Logger::out(const QString &message)
{
    instance()->log("", "", message);
}

void Logger::notice(const QString &className, const QString &message)
{
    instance()->log(className, "NOTICE", message);
}

void Logger::warning(const QString &className, const QString &message)
{
    instance()->log(className, "WARNING", message);
}

void Logger::error(const QString &className, const QString &message)
{
    instance()->log(className, "ERROR", message);
}

// private
Logger::Logger()
{
    consoleDebugEnabled = false;
    logFileEnabled = false;

    QDir logDir;
    logDir.setCurrent(QCoreApplication::applicationDirPath());
    //logDir.setCurrent(qApp->applicationDirPath());
    if(!logDir.exists() ) {
        logDir.mkpath(".");
    }
    //qDebug() << "path: " << logDir.currentPath();
    QString logFileLocation = logDir.currentPath() + "/log.txt";

    //logFile = QFile::(logFileLocation);
    logFile.setFileName(logFileLocation);
    bool result = logFile.open(QIODevice::WriteOnly);
    if(!result) {
        logFileEnabled = false;
        log("Logger", "Warning", "Could not open Log");
        return;
    }
    if(!appendEnabled)
        logFile.resize(0);
}

void Logger::log(const QString &className, const QString &type, const QString &message)
{
    QMutex mutex;
    mutex.lock();
    QString logMessage;
    if(className.isEmpty() && type.isEmpty() ) {
        logMessage = QString("%1").arg(message);
    }
    else if (className.isEmpty()) {
        logMessage = QString("%1: " + message).arg(type);
    } else {
        logMessage = QString("%1: %2: " + message).arg(className).arg(type);
    }

    if (logMessage.endsWith("\n")) {
        logMessage = logMessage.left(logMessage.size() - 1);
    }

    QByteArray byteArray = logMessage.toUtf8();
    if (byteArray.size() >= 8192) {
        byteArray.truncate(8191);
        byteArray[8191] = '\0';
    }

    if(consoleDebugEnabled)
        qDebug("%s", byteArray.constData());

    /*
    Q_ASSERT(m_d);
    if (m_d->logEnabled) {
        m_d->logFile->write((logMessage + "\n").toUtf8());
        m_d->logFile->flush();
    }
    */

    if(logFileEnabled) {
        logFile.write((logMessage + "\n").toUtf8());
        logFile.flush();
        //qDebug() << "write log";
    }
    mutex.unlock();
}








