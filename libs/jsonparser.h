#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QObject>
#include <QVariant>

class Logger;

class JsonParser : public QObject
{
    Q_OBJECT
public:
    explicit JsonParser();
protected:
    bool parseJsonResponse(const QString &jsonResponse);
    virtual QVariant parseJsonObject(const QJsonObject &object);
    virtual QVariant parseJsonArray(const QJsonArray &array);
    virtual QVariant parseJsonValue(const QJsonValue &value, const QString &keyName);
    Logger *logger;
private:
    virtual void parseString(const QString &value, const QString &keyName) = 0;
    virtual void parseDouble(const double &value, const QString &keyName) = 0;
    virtual void parseBool(const bool &value, const QString &keyName) = 0;
};

#endif // JSONPARSER_H











