#ifndef LOGGER_H
#define LOGGER_H

#include <QString>
#include <QFile>

#define DEBUG_FUNCTION_NAME  QString("%1").arg(__PRETTY_FUNCTION__, -70, QLatin1Char(' '))

class Logger
{
public:
    static Logger *instance();
    static void destroy();
    void enableConsoleDebug(bool enabled);
    void enableLogFileDebug(bool enabled);
    void enableLogFileAppend(bool enabled);

    void debug(const QString &className, const QString &message);
    void out(const QString &message);
    void notice(const QString &className, const QString &message);
    void warning(const QString &className, const QString &message);
    void error(const QString &className, const QString &message);
private:
    Logger();
    ~Logger() {}
    Logger(const Logger&);                 // Prevent copy-construction
    Logger& operator=(const Logger&);

    void log(const QString &className, const QString &type, const QString &message);
    static Logger *s_instance;
    QFile logFile;
    bool consoleDebugEnabled;
    bool logFileEnabled;
    bool appendEnabled;
};

#endif // LOGGER_H
