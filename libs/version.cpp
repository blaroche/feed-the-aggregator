#include <QStringList>
#include "version.h"

Version::Version()
{
    major = 0;
    minor = 0;
    patch = 0;
}

Version::Version(const Version &version)
{
    major = version.getMajor();
    minor = version.getMinor();
    patch = version.getPatch();
}

Version::Version(const QString &version)
{
    QStringList parts = version.split('.');
    major = parts.size() > 0 ? parts[0].toInt() : 0;
    minor = parts.size() > 1 ? parts[1].toInt() : 0;
    patch = parts.size() > 2 ? parts[2].toInt() : 0;
}

QString Version::toString() const
{
    return QString::number(major) + "." +
           QString::number(minor) + "." +
           QString::number(patch);
}

QString Version::toSimpleString() const
{
    if(patch == 0 && minor == 0)
        return QString::number(major);
    else if(patch == 0)
        return QString::number(major) + "." +
               QString::number(minor);
    else
        return QString::number(major) + "." +
               QString::number(minor) + "." +
               QString::number(patch);
}

void Version::clear()
{
    major = 0;
    minor = 0;
    patch = 0;
}


// operators
Version &Version::operator=(const Version &version)
{
    major = version.major;
    minor = version.minor;
    patch = version.patch;
    return *this;
}

Version &Version::operator=(const QString &version)
{
    return operator=(Version(version));
}

bool Version::operator==(const Version &version) const
{
    return (major == version.major && minor == version.minor && patch == version.patch);
}

bool Version::operator!=(const Version &version) const
{
    return (major != version.major || minor != version.minor || patch != version.patch);
}

bool Version::operator>=(const Version &version) const
{
    return (major >  version.major) ||
           (major == version.major && minor >  version.minor) ||
           (major == version.major && minor == version.minor && patch >= version.patch);
}

bool Version::operator>(const Version &version) const
{
    return (major >  version.major) ||
           (major == version.major && minor >  version.minor) ||
           (major == version.major && minor == version.minor && patch > version.patch);
}

bool Version::operator!() const
{
    return (!major && !minor && !patch);
}


// setters
void Version::setMajor(int major)
{
    this->major = major;
}

void Version::setMinor(int minor)
{
    this->minor = minor;
}

void Version::setPatch(int patch)
{
    this->patch = patch;
}


// getters
int Version::getMajor() const
{
    return major;
}

int Version::getMinor() const
{
    return minor;
}

int Version::getPatch() const
{
    return patch;
}

