// qt
#include <QDebug>
#include <QUrl>

// local
#include "networkaccessmanager.h"
#include "logger.h"

NetworkAccessManager::NetworkAccessManager()
{
    logger = Logger::instance();
    error = false;
    httpRequestAborted = false;
}

NetworkAccessManager::~NetworkAccessManager()
{
    logger = 0;
}

// public slots

// private slots
void NetworkAccessManager::cancelRequest()
{
    logger->debug(DEBUG_FUNCTION_NAME, "timeout reached, aborting reply");
    reply->abort();
    httpRequestAborted = true;
}

void NetworkAccessManager::httpFinished()
{
    if(httpRequestAborted) {
        logger->debug(DEBUG_FUNCTION_NAME, "http request aborted");
        error = true;
    }

    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    //logger->debug(DEBUG_FUNCTION_NAME, QString("status code: %1").arg(statusCode.toString()));

    if(reply->error()) {
        error = true;
        errorMessage = reply->errorString();
        logger->debug(DEBUG_FUNCTION_NAME, errorMessage);

        QByteArray bytes = reply->readAll();
        result = QString::fromUtf8(bytes);
        if(requestType == Get)
            emit getReplyReady(Error);
    }
    else if(requestType == Get) {
        if(getType == GetString) {
            //QByteArray bytes = reply->readAll();
            result = reply->readAll();
            //result = QString::fromUtf8(bytes);
            emit getReplyReady(GetString);
        }
        else if(getType == GetTempFile) {
            temporaryFile.open();
            temporaryFile.write(reply->readAll());
            temporaryFile.flush();
            temporaryFile.setPermissions(QFile::ExeOther | QFile::ReadOther);
            emit getReplyReady(GetTempFile);
        }
    }
    content.clear();
    reply->deleteLater();
    delete request;
    eventloop->exit();
}

void NetworkAccessManager::httpReadyRead()
{
    result += reply->readAll();
}

void NetworkAccessManager::httpError(QNetworkReply::NetworkError error )
{
    this->error = true;
    logger->debug(DEBUG_FUNCTION_NAME, QString("error %1").arg(error));
}

void NetworkAccessManager::sslError(const QList<QSslError> &errorList)
{
    Q_UNUSED(errorList);
    //foreach(QSslError error, errorList)
    //    logger->debug("NetworkAccessManager::sslError", QString("error %1").arg(error.errorString()));
    //logger->debug("NetworkAccessManager::sslError", "got ssl error(s)");
    reply->ignoreSslErrors();
}


// protected
void NetworkAccessManager::printResult() const
{
    qDebug() << "Results: " << result;
}

void NetworkAccessManager::startRequest(RequestType requestType, GetType getType)
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    //logger->debug(DEBUG_FUNCTION_NAME, url.toString());
    this->requestType = requestType;
    this->getType = getType;

    result.clear();
    error = false;
    httpRequestAborted = false;

    request = new QNetworkRequest();
    request->setUrl(url);
    setRequestHeader();

    QSslConfiguration sslConfig = request->sslConfiguration();
    sslConfig.setPeerVerifyMode(QSslSocket::AutoVerifyPeer);
    sslConfig.setProtocol(QSsl::SslV3);

    request->setSslConfiguration(sslConfig);

    if(this->requestType == Post) {
        //logger->debug(DEBUG_FUNCTION_NAME, "POSTING request");
        reply = this->post(*request, content);
    }
    else if(this->requestType == Get) {
        //logger->debug(DEBUG_FUNCTION_NAME, "GETTING request");
        reply = this->get(*request);
    }

    QTimer timer;
    timer.setSingleShot(true);

    eventloop = new QEventLoop();
    timer.start(30000);
    connect(reply, SIGNAL(finished()), this, SLOT(httpFinished()));
    if(this->requestType == Post)
        connect(reply, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(httpError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(const QList<QSslError> &)), this, SLOT(sslError(const QList<QSslError> &)));
    connect(&timer, SIGNAL(timeout()), this, SLOT(cancelRequest()));
    eventloop->exec();

    delete eventloop;
}

void NetworkAccessManager::setRequestHeader()
{
    logger->debug("NetworkAccessManager::setRequestHeader", "called");
    //if(requestType == Post)
    //    request->setRawHeader("Content-Type", "application/json");
    //else if(requestType == Get)
    //    request->setRawHeader("Accept", "application/json");
    request->setRawHeader("Content-Type", "application/json");
    request->setRawHeader("Accept", "application/json");
    //request->setRawHeader(QString("Content-Length").toLatin1(), QString::number(content.length()).toLatin1());
    request->setRawHeader(QString("Content-Length").toLatin1(), QByteArray::number(content.size()));
}

void NetworkAccessManager::clear()
{
    logger->debug(DEBUG_FUNCTION_NAME, "called");
    content.clear();
    content.squeeze();
    result.clear();
    result.squeeze();
}

// setters
void NetworkAccessManager::setContent(const QString &content)
{
    this->content.clear();
    this->content.append(content);
}

void NetworkAccessManager::setContent(const QByteArray &content)
{
    this->content.clear();
    this->content.append(content);
}

void NetworkAccessManager::setUrl(const QString &url)
{
    //this->url.setUrl(url);
    this->url = url;
}

void NetworkAccessManager::setUrl(const QUrl &url)
{
    this->url = url;
}

void NetworkAccessManager::setUrl(const QByteArray &url)
{
    this->url = QUrl::fromEncoded(url);
}

void NetworkAccessManager::setErrorMessage(const QString &message)
{
    errorMessage = message;
}

// getters
QString NetworkAccessManager::getErrorMessage() const
{
    return errorMessage;
}

QString NetworkAccessManager::getResult() const
{
    return result;
}

QString& NetworkAccessManager::getResult()
{
    return result;
}

bool NetworkAccessManager::getError() const
{
    return error;
}

QNetworkRequest* NetworkAccessManager::getRequest() const
{
    return request;
}

QNetworkReply* NetworkAccessManager::getReply() const
{
    return reply;
}

QByteArray NetworkAccessManager::getContent() const
{
    return content;
}

// private










