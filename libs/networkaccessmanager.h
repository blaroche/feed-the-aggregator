#ifndef NETWORKACCESSMANAGER_H
#define NETWORKACCESSMANAGER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QtNetwork>

class Logger;
class QNetworkReply;
class QNetworkRequest;

class NetworkAccessManager : public QNetworkAccessManager
{
    Q_OBJECT
public:
    enum RequestType { Get, Post };
    enum GetType { None, Error, GetString, GetTempFile };
    NetworkAccessManager();
    ~NetworkAccessManager();
signals:
    void getReplyReady(GetType type);
private slots:
    void cancelRequest();
    void httpFinished();
    void httpReadyRead();
    void httpError(QNetworkReply::NetworkError error);
    void sslError(const QList<QSslError> &errorList);
protected:
    void printResult() const;
    void startRequest(RequestType requestType = Post, GetType getType = None);
    virtual void setRequestHeader();
    void clear();

    //setters
    void setContent(const QString &content);
    void setContent(const QByteArray &content);
    void setUrl(const QString &url);
    void setUrl(const QUrl &url);
    void setUrl(const QByteArray &url);
    void setErrorMessage(const QString &message);
    //getters
    QString getErrorMessage() const;
    QString getResult() const;
    QString& getResult();
    bool getError() const;
    QNetworkRequest* getRequest() const;
    QNetworkReply* getReply() const;
    QByteArray getContent() const;

    Logger *logger;
    QTemporaryFile temporaryFile;
private:
    QNetworkReply *reply;
    QEventLoop *eventloop;
    QNetworkRequest *request;

    QUrl url;
    QByteArray content;
    QString result;
    QString errorMessage;
    bool httpRequestAborted;
    bool error;
    RequestType requestType;
    GetType getType;

    //static int instances;
};

#endif // NETWORKACCESSMANAGER_H
