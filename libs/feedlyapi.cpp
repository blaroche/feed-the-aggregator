#include <QDebug>
#include <QSettings>
#include <QDateTime>

#include "feedlyapi.h"
#include "config.h"
#include "libs/logger.h"

FeedlyApi::FeedlyApi()
{
    logger = Logger::instance();
    //connect(&parser, SIGNAL(gotAccesstoken(const QString&)),this, SLOT(gotAccessToken(const QString &)));
    willParseResults = true;
}

bool FeedlyApi::authRequest()
{
    QString url;
    url = FeedlyAuth;

    QString args;
    args = QString("?response_type=code&client_id=%1&redirect_uri=%2").arg(ClientId).arg(RedirectURL);
    args.append(QString("&scope=%1").arg(Scope)  );
    url.append(args);

    //logger->debug(DEBUG_FUNCTION_NAME, QString("Url: %1").arg(url));
    setUrl(url);

    logger->out("******************* beginAuth **************************");
    logger->out("******************* ********* **************************");
    startRequest(Get, GetString);

    if(getError()) {
        //TODO::handle error
        return false;
    }

    //qDebug() << "getResults: " << getResult();
    emit loadAuthorizationPage(getResult(), FeedlyBase);

    return true;
}

bool FeedlyApi::profileRequest()
{
    QString url;
    url = FeedlyProfile;
    setUrl(url);

    logger->out("******************* getProfile **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::Profile);

    return true;
}

bool FeedlyApi::preferencesRequest()
{
    QString url;
    url = FeedlyPreferences;
    setUrl(url);

    logger->out("******************* getPreferences **************************");
    logger->out("******************* ************** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::Preferences);

    return true;
}

bool FeedlyApi::categoriesRequest()
{
    QString url;
    url = FeedlyCategories;
    setUrl(url);

    logger->out("******************* getCategories **************************");
    logger->out("******************* ************* **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::Categories);
    return true;
}

bool FeedlyApi::subscriptionsRequest()
{
    QString url;
    url = FeedlySubscriptions;
    setUrl(url);

    logger->out("******************* getSubscriptions **************************");
    logger->out("******************* **************** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::Subscriptions);
    return true;
}

bool FeedlyApi::markersRequest()
{
    QString url;
    url = FeedlyMarkers;
    setUrl(url);

    logger->out("******************* getMarkers **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::Markers);
    return true;
}

bool FeedlyApi::entriesMgetFromFeedId(const QString &feedId)
{
    QString url;
    url = FeedlyStream;
    url.append(QUrl::toPercentEncoding(feedId));
    setUrl(url);

    //qDebug() << "feedId: " << feedId;
    logger->out("******************* getStream **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //qDebug() << "results: " << getResult();

    url = FeedlyEntriesMget;
    setUrl(url);
    setContent(getResult());

    logger->out("******************* getEntriesMget **************************");
    logger->out("******************* ************** **************************");
    startRequest(Post, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //printResult();
    if(willParseResults)
        parser.parseResponse(getResult(), FeedlyParser::EntriesMget);
    return true;
}


// depreciated methods
bool FeedlyApi::beginAuth()
{
    QString url;
    url = FeedlyAuth;

    QString args;
    args = QString("?response_type=code&client_id=%1&redirect_uri=%2").arg(ClientId).arg(RedirectURL);
    args.append(QString("&scope=%1").arg(Scope)  );
    //args = args.toHtmlEscaped();
    url.append(args);
    //url.append(QUrl::toPercentEncoding(args));


    //logger->debug(DEBUG_FUNCTION_NAME, QString("Url: %1").arg(url));
    setUrl(url);


    logger->out("******************* beginAuth **************************");
    logger->out("******************* ********* **************************");
    startRequest(Get, GetString);

    if(getError()) {
        //TODO::handle error
        return false;
    }

    //qDebug() << "getResults: " << getResult();
    emit loadAuthorizationPage(getResult(), FeedlyBase);
    return true;
}

bool FeedlyApi::getProfile()
{
    QString url;
    url = FeedlyProfile;
    setUrl(url);

    logger->out("******************* getProfile **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    parser.parseResponse(getResult(), FeedlyParser::Profile);
    return true;
}

bool FeedlyApi::getPreferences()
{
    QString url;
    url = FeedlyPreferences;
    setUrl(url);

    logger->out("******************* getPreferences **************************");
    logger->out("******************* ************** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    parser.parseResponse(getResult(), FeedlyParser::Preferences);
    return true;
}

bool FeedlyApi::getCategories()
{
    QString url;
    url = FeedlyCategories;
    setUrl(url);

    logger->out("******************* getCategories **************************");
    logger->out("******************* ************* **************************");
    startRequest(Get, GetString);

    if(getError()) {
        parser.parseResponse(getResult(), FeedlyParser::Categories);
        handleError();
        return false;
    }
    parser.parseResponse(getResult(), FeedlyParser::Categories);
    return true;
}

bool FeedlyApi::getSubscriptions()
{
    QString url;
    url = FeedlySubscriptions;
    setUrl(url);

    logger->out("******************* getSubscriptions **************************");
    logger->out("******************* **************** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //printResult();
    parser.parseResponse(getResult(), FeedlyParser::Subscriptions);


    return true;
}

bool FeedlyApi::getMarkers()
{
    QString url;
    url = FeedlyMarkers;
    setUrl(url);

    logger->out("******************* getMarkers **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //printResult();
    parser.parseResponse(getResult(), FeedlyParser::Markers);
    return true;
}

bool FeedlyApi::getStream(const QString &id)
{
    QString url;
    url = FeedlyStream;

    url.append(id);
    setUrl(url);

    logger->out("******************* getStream **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    printResult();
    logger->out("******************* ********** **************************");
    logger->out("******************* ********** **************************");
    logger->out("******************* ********** **************************");
    //parser.parseResponse(getResult(), FeedlyParser::Markers);
    return true;
}

bool FeedlyApi::getEntriesMgetFromFeedId(const QString &feedId)
{
    QString url;
    url = FeedlyStream;
    url.append(QUrl::toPercentEncoding(feedId));
    setUrl(url);

    //qDebug() << "feedId: " << feedId;
    logger->out("******************* getStream **************************");
    logger->out("******************* ********** **************************");
    startRequest(Get, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //qDebug() << "results: " << getResult();

    url = FeedlyEntriesMget;
    setUrl(url);
    setContent(getResult());

    logger->out("******************* getEntriesMget **************************");
    logger->out("******************* ************** **************************");
    startRequest(Post, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    //printResult();
    parser.parseResponse(getResult(), FeedlyParser::EntriesMget);
    return true;
}

bool FeedlyApi::markEntryAsRead(const QString &id)
{
    QString url;
    url = FeedlyMarkEntryRead;

    setUrl(url);
    //TODO:: need to get to work on a json generator
    QString jsonString = QString("{ \"action\": \"markAsRead\", \"type\": \"entries\", \"entryIds\": [\"%1\"]}").arg(id);

    qDebug() << "jsonString: " << jsonString;


    setContent(jsonString);
    logger->out("******************* MarkEntryAsRead **************************");
    logger->out("******************* *************** **************************");
    startRequest(Post, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    printResult();
    return true;
}

bool FeedlyApi::markFeedAsRead(const QString &id)
{
    QString url;
    url = FeedlyMarkFeedRead;

    setUrl(url);

    //TODO:: need to get to work on a json generator
    //QString jsonString = QString("{ \"action\": \"markAsRead\", \"type\": \"feeds\", \"feedIds\": [\"%1\"], \"lastReadEntryId\": \"%2\" }").arg(id);
    QString jsonString = QString("{ \"action\": \"markAsRead\", \"type\": \"feeds\", \"feedIds\": [\"%1\"], \"asOf\": \"%2\" }").arg(id).arg(QDateTime::currentMSecsSinceEpoch());
    /*
    {
      "action": "markAsRead",
      "type": "feeds",
      "feedIds": [
        "feed/http://feeds.feedburner.com/design-milk"
      ],
      "lastReadEntryId": "c805fcbf-3acf-4302-a97e-d82f9d7c897f"
    }
    */

    qDebug() << "jsonString: " << jsonString;


    setContent(jsonString);
    logger->out("******************* MarkFeedAsRead **************************");
    logger->out("******************* *************** **************************");
    startRequest(Post, GetString);

    if(getError()) {
        handleError();
        return false;
    }
    printResult();
    return true;
}
// depreciated methods

void FeedlyApi::parseResults(const bool &onOff)
{
    willParseResults = onOff;
}

QString FeedlyApi::printResults()
{
    return getResult();
}

QNetworkReply::NetworkError FeedlyApi::getFeedlyError() const
{
    return getReply()->error();
}

FeedModel* FeedlyApi::getFeedsModel() const
{
    return parser.getFeedsModel();
}

EntriesModel* FeedlyApi::getEntriesModel() const
{
    return parser.getEntriesModel();
}


void FeedlyApi::parseAuthorizationResponse(const QString &response)
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QString code;
    QString state;
    QString tmp = response;
    tmp.remove("http://localhost/?");
    QStringList parms = tmp.split("&");

    foreach(QString parm, parms) {
        if(parm.contains("code"))
            code = parm;
        else if(parm.contains("state"))
            state = parm;
    }
    code.remove("code=");
    state.remove("state=");

    logger->debug(DEBUG_FUNCTION_NAME, QString("code: %1").arg(code));
    //logger->debug(DEBUG_FUNCTION_NAME, QString("state: %1").arg(state));
    getAuthorizationToken(code, state);
}

// setters
void FeedlyApi::setAccessToken(const QString &token)
{
    mAccessToken = token;
}

// slots
void FeedlyApi::gotAccessToken(const QString &accessToken)
{
    logger->debug(DEBUG_FUNCTION_NAME, accessToken);
    mAccessToken = accessToken;
    QSettings settings;
    settings.setValue("access_token", accessToken);
    emit authCompleted();
}

// private
void FeedlyApi::getAuthorizationToken(const QString &code, const QString &state)
{
    logger->debug(DEBUG_FUNCTION_NAME, "entered");
    QString url;
    url = FeedlyAuthToken;
    url = url.replace("http", "https");

    QString args;
    args = QString("?code=%1&client_id=%2").arg(code).arg(ClientId);
    args.append(QString("&client_secret=%1&redirect_uri=%3").arg(ClientSecret).arg(RedirectURL));

    if(!state.isEmpty())
        args.append(QString("&state=%1").arg(state));
    args.append("&grant_type=authorization_code");
    url.append(args);

    setUrl(url);
    logger->debug(DEBUG_FUNCTION_NAME, QString("Url: %1").arg(url));
    startRequest(Post);
    printResult();

    if(!getError())
        parser.parseResponse(getResult(), FeedlyParser::AuthToken);
    emit authCompleted();
}

void FeedlyApi::setRequestHeader()
{
    //logger->debug(DEBUG_FUNCTION_NAME, "entered");
    getRequest()->setRawHeader("Content-Type", "application/json");
    getRequest()->setRawHeader("Accept", "application/json");
    getRequest()->setRawHeader(QString("Content-Length").toLatin1(), QByteArray::number(getContent().size()));
    if(!mAccessToken.isEmpty()) {
        if(!mAccessToken.startsWith("OAuth"))
            mAccessToken.prepend("OAuth ");
        getRequest()->setRawHeader(QByteArray("Authorization"), mAccessToken.toLatin1());
    }
    //qDebug() << "access token: " << mAccessToken;
}

void FeedlyApi::handleError()
{
    switch(getReply()->error()) {
    case QNetworkReply::AuthenticationRequiredError:
        logger->debug(DEBUG_FUNCTION_NAME, "auth error, begin new auth");
        //beginAuth();
        break;
    default:
        logger->debug(DEBUG_FUNCTION_NAME, QString("unhandled error: %1").arg(getReply()->errorString()));
        break;
    }

}
























